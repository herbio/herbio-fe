# HerbIO - Application

Web and Android Application for the Herbio Team Project.

## Getting Started 

### Prerequisites 

- #### Flutter SDK
	- Follow the [Installation Guide](https://docs.flutter.dev/get-started/install?gclsrc=ds&gclsrc=ds) on the Official Flutter Website.
  
## Deployment

Create a new file with the name `server_path.dart` in the `/lib/constatns/` folder. Specify your backend server IP Address the following way:

```dart
const String SERVER = 'YOUR_SERVER_IP_ADDRESS';
```

### Web
Once the backend server IP Address is defined run the following commands:

```bash
flutter clean; flutter build web;
```

Wait for the process to finish. After the process is done replace the `<base href="/">` line in the `build/web/index.html` file with the following line:

```html
<base href="./">
```

Deploy the content of the `build/web` folder to your server.

### Mobile (Android)

Once the backend server IP Address is defined run the following commands:

```bash
flutter clean; flutter build apk;
```

Wait for the process to finish. After the process is done copy the `build/app/outputs/flutter-apk/app-release.apk` APK file to your Android smartphone. Allow `App Installations from Unknown Sources` and install the APK file.


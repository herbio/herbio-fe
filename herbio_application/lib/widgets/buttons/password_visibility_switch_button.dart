import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

class PasswordVisibilitySwitchButton extends StatelessWidget {
  final bool _passwordVisibility;
  final Function _switchFunction;
  final FocusNode _focusNode;

  const PasswordVisibilitySwitchButton(
    this._passwordVisibility,
    this._switchFunction,
    this._focusNode,
  );

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        right: 8.0,
      ).r,
      child: IconButton(
        iconSize: 25.r,
        icon: _passwordVisibility
            ? Icon(Icons.visibility)
            : Icon(Icons.visibility_off),
        tooltip: _passwordVisibility ? 'Show Password' : 'Hide Password',
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        color: _focusNode.hasFocus
            ? Theme.of(context).colorScheme.secondary
            : const Color(0xFF898989),
        onPressed: () {
          _switchFunction();
        },
      ),
    );
  }
}

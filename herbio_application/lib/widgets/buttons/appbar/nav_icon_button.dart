import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../constants/asset_constants.dart';
import '../../../constants/url_constants.dart';
import '../../../utils/utility_functions.dart';

class NavIconButton extends StatefulWidget {
  final IconData? _icon;
  final Color _color;
  final Color _switchColor;
  final String _tooltip;
  final Function? _onPressed;

  const NavIconButton(this._tooltip, this._color, this._switchColor,
      [this._icon, this._onPressed]);

  @override
  _NavIconButtonState createState() => _NavIconButtonState(
        this._color,
        0,
        0,
        0.0,
        0.0,
      );
}

class _NavIconButtonState extends State<NavIconButton> {
  Color _textColor;
  int _enterCounter;
  int _exitCounter;
  double x;
  double y;

  _NavIconButtonState(
    this._textColor,
    this._enterCounter,
    this._exitCounter,
    this.x,
    this.y,
  );

  void _incrementEnter(PointerEvent details) {
    setState(
      () {
        this._enterCounter++;
      },
    );
  }

  void _incrementExit(PointerEvent details) {
    setState(
      () {
        _textColor = widget._color;
        this._exitCounter++;
      },
    );
  }

  void _updateLocation(PointerEvent details) {
    setState(
      () {
        _textColor = widget._switchColor;
        x = details.position.dx;
        y = details.position.dy;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: _incrementEnter,
      onHover: _updateLocation,
      onExit: _incrementExit,
      child: IconButton(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        icon: widget._icon == null
            ? SvgPicture.asset(
                GIT_LAB_ICON,
                color: _textColor,
              )
            : Icon(
                widget._icon,
                color: _textColor,
                size: 25.0.sp,
              ),
        tooltip: widget._tooltip,
        onPressed: () {
          if (widget._onPressed == null) {
            launchURL(GIT_LAB);
          } else {
            widget._onPressed!();
          }
        },
      ),
    );
  }
}

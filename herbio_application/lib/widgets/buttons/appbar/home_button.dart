import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:routemaster/routemaster.dart';

class HomeButton extends StatelessWidget {
  final String path;

  HomeButton({required this.path});

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: ButtonStyle(
        overlayColor: MaterialStateColor.resolveWith(
          (states) => Colors.transparent,
        ),
      ),
      child: RichText(
        text: TextSpan(
          text: "Herb",
          style: TextStyle(
            fontSize: 28.sp,
            color: Theme.of(context).primaryColorLight,
          ),
          children: <TextSpan>[
            TextSpan(
              text: "IO",
              style: TextStyle(
                fontSize: 28.sp,
                color: Theme.of(context).colorScheme.secondary,
              ),
            ),
          ],
        ),
      ),
      onPressed: () {
        Routemaster.of(context).replace(path);
      },
    );
  }
}

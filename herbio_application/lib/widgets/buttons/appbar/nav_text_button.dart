import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:routemaster/routemaster.dart';

import '../../../modules/screens/screen.dart';

class NavTextButton extends StatefulWidget {
  final String text;
  final Screen screen;

  const NavTextButton({Key? key, required this.text, required this.screen})
      : super(key: key);

  @override
  _NavTextButtonState createState() => _NavTextButtonState();
}

class _NavTextButtonState extends State<NavTextButton> {
  Color _textColor = Colors.white;
  int _enterCounter = 0;
  int _exitCounter = 0;
  double x = 0.0;
  double y = 0.0;

  void _incrementEnter(PointerEvent details) {
    setState(
      () {
        this._enterCounter++;
      },
    );
  }

  void _incrementExit(PointerEvent details) {
    setState(
      () {
        _textColor = Theme.of(context).primaryColorLight;
        this._exitCounter++;
      },
    );
  }

  void _updateLocation(PointerEvent details) {
    setState(
      () {
        _textColor = Theme.of(context).colorScheme.secondary;
        x = details.position.dx;
        y = details.position.dy;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: _incrementEnter,
      onHover: _updateLocation,
      onExit: _incrementExit,
      child: TextButton(
        style: ButtonStyle(
          overlayColor: MaterialStateColor.resolveWith(
            (states) => Colors.transparent,
          ),
        ),
        child: Text(
          this.widget.text,
          style: TextStyle(
            fontSize: 16.sp,
            color: _textColor,
          ),
        ),
        onPressed: () {
          Routemaster.of(context).replace(widget.screen.routeName);
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';

import '../../../../modules/models/entities/water_tank.dart';
import '../components/liquid_water_level_indicator.dart';
import '../components/row_data_display.dart';

class WaterTankCardWebLayout extends StatelessWidget {
  final WaterTank waterTank;

  WaterTankCardWebLayout({
    required this.waterTank,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(
          25,
        ),
      ),
      margin: EdgeInsets.symmetric(
        vertical: 7,
        horizontal: 15,
      ).r,
      elevation: 5,
      child: FittedBox(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 10.h,
            ),
            Center(
              child: Text(
                'Water Level',
                style: TextStyle(
                  fontSize: 25.0.sp,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(
                30.0,
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  LiquidWaterLevelIndicator(waterTank: waterTank),
                  SizedBox(
                    width: 30.w,
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      RowDataDisplay(
                        dataName: 'Capacity: ',
                        dataValue:
                            '${waterTank.capacity.toStringAsFixed(2)} ml',
                      ),
                      RowDataDisplay(
                        dataName: 'Current Level: ',
                        dataValue: '${waterTank.waterLevel * 100} %',
                      ),
                      RowDataDisplay(
                        dataName: 'Last time Filled: ',
                        dataValue: '${DateFormat('yyyy-MM-dd HH:mm').format(
                          waterTank.lastTimeFilled,
                        )}',
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

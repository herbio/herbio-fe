import 'package:flutter/material.dart';

import 'package:intl/intl.dart';

import '../../../../modules/models/entities/water_tank.dart';
import '../components/liquid_water_level_indicator.dart';
import '../components/row_data_display.dart';

class WaterTankCardAndroidLayout extends StatelessWidget {
  final WaterTank waterTank;

  WaterTankCardAndroidLayout({
    required this.waterTank,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(
          25,
        ),
      ),
      margin: EdgeInsets.symmetric(
        vertical: 7,
        horizontal: 15,
      ),
      elevation: 5,
      child: FittedBox(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 10,
            ),
            Center(
              child: Text(
                'Water Level',
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(
                30.0,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  LiquidWaterLevelIndicator(waterTank: waterTank),
                  SizedBox(
                    height: 30,
                  ),
                  RowDataDisplay(
                    dataName: 'Capacity: ',
                    dataValue: '${waterTank.capacity.toStringAsFixed(2)} ml',
                  ),
                  RowDataDisplay(
                    dataName: 'Current Level: ',
                    dataValue: '${waterTank.waterLevel * 100} %',
                  ),
                  RowDataDisplay(
                    dataName: 'Last time Filled: ',
                    dataValue:
                        '${DateFormat('yyyy-MM-dd').format(waterTank.lastTimeFilled)}',
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

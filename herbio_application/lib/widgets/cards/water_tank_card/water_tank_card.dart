import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../../modules/models/entities/water_tank.dart';
import 'layouts/water_tank_card_android_layout.dart';
import 'layouts/water_tank_card_web_layout.dart';

class WaterTankCard extends StatelessWidget {
  final WaterTank waterTank;

  WaterTankCard({
    required this.waterTank,
  });

  @override
  Widget build(BuildContext context) {
    return kIsWeb
        ? WaterTankCardWebLayout(
            waterTank: waterTank,
          )
        : WaterTankCardAndroidLayout(
            waterTank: waterTank,
          );
  }
}

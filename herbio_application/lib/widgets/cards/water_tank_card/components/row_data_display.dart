import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

class RowDataDisplay extends StatelessWidget {
  final String dataName;
  final String dataValue;

  RowDataDisplay({
    required this.dataName,
    required this.dataValue,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              dataName,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15.0.sp,
              ),
            ),
            Text(
              dataValue,
              style: TextStyle(
                fontSize: 15.0.sp,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }
}

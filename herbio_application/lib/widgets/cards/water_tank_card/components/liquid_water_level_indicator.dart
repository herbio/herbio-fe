import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:path_drawing/path_drawing.dart';

import '../../../../constants/app_constants.dart';
import '../../../../modules/models/entities/water_tank.dart';

class LiquidWaterLevelIndicator extends StatelessWidget {
  final WaterTank waterTank;

  LiquidWaterLevelIndicator({
    required this.waterTank,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 300.h,
          width: 300.w,
          child: FittedBox(
            child: LiquidCustomProgressIndicator(
              value: waterTank.waterLevel,
              valueColor: AlwaysStoppedAnimation(
                Colors.blue,
              ),
              backgroundColor: Colors.black,
              direction: Axis.vertical,
              shapePath: parseSvgPathData(
                BARREL_SVG_STRING,
              ),
            ),
          ),
        ),
        SizedBox(
          width: 70.w,
        ),
      ],
    );
  }
}

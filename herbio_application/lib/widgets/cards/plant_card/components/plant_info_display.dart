import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../constants/asset_constants.dart';
import '../../../../modules/models/entities/plant.dart';
import '../../../../modules/models/types/watering_mode.dart';
import '../../../../utils/services/indicator_service.dart';
import '../../water_tank_card/components/row_data_display.dart';

class PlantInfoDisplay extends StatelessWidget {
  final Plant plant;
  final List<double> boxSizes;

  const PlantInfoDisplay({
    required this.plant,
    required this.boxSizes,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              this.plant.wateringMode == WateringMode.MANUAL
                  ? SizedBox(
                      height: 10.h,
                    )
                  : Container(),
              Card(
                elevation: 5,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(
                    15.0,
                  ),
                ),
                child: Container(
                  height: 275.h,
                  width: 175.w,
                  child: Image.asset(
                    PLANT,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          width: 20.w,
        ),
        Expanded(
          flex: 1,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: this.boxSizes[0].h,
              ),
              FittedBox(
                child: RowDataDisplay(
                  dataName: 'Name: ',
                  dataValue: this.plant.plantName,
                ),
              ),
              FittedBox(
                child: RowDataDisplay(
                  dataName: 'Water Requirement: ',
                  dataValue: '${this.plant.wateringRequirement.toString()} ml',
                ),
              ),
              FittedBox(
                child: Text(
                  'Lighting Requirements',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(
                height: this.boxSizes[1],
              ),
              FittedBox(
                child: Row(
                  children: generateLightingLevel(
                    this.plant.lightingRequirement,
                  ),
                ),
              ),
              SizedBox(
                height: this.boxSizes[2],
              ),
              RowDataDisplay(
                dataName: 'Humidity: ',
                dataValue: '${this.plant.humidity}',
              ),
              FittedBox(
                child: RowDataDisplay(
                  dataName: 'Soil Temperature: ',
                  dataValue: '${this.plant.soilTemperature} °C',
                ),
              ),
              FittedBox(
                child: RowDataDisplay(
                  dataName: 'Watering Mode: ',
                  dataValue:
                      '${this.plant.wateringMode.toString().split('.')[1]}',
                ),
              ),
              this.plant.wateringMode == WateringMode.AUTO
                  ? Column(
                      children: [
                        FittedBox(
                          child: RowDataDisplay(
                            dataName: 'Watering Amount: ',
                            dataValue: '${this.plant.wateringAmount} ml',
                          ),
                        ),
                        FittedBox(
                          child: RowDataDisplay(
                            dataName: 'Lower Watering Threshold: ',
                            dataValue:
                                '${this.plant.lowerWaterThreshold.toStringAsFixed(2)}',
                          ),
                        ),
                        FittedBox(
                          child: RowDataDisplay(
                            dataName: 'Upper Watering Threshold: ',
                            dataValue:
                                '${this.plant.upperWaterThreshold.toStringAsFixed(2)} ',
                          ),
                        ),
                      ],
                    )
                  : Container(),
            ],
          ),
        )
      ],
    );
  }
}

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../modules/models/entities/plant.dart';
import 'components/plant_info_display.dart';

class PlantCard extends StatefulWidget {
  final Plant plant;

  PlantCard({
    required this.plant,
  });

  @override
  State<PlantCard> createState() => _PlantCardState();
}

class _PlantCardState extends State<PlantCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(
          25,
        ),
      ),
      margin: EdgeInsets.symmetric(
        vertical: 7,
        horizontal: 15,
      ).r,
      elevation: 5,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 20.0,
          vertical: 30,
        ).r,
        child: kIsWeb
            ? PlantInfoDisplay(
                plant: widget.plant,
                boxSizes: [30.0, 10.0, 25.0],
              )
            : PlantInfoDisplay(
                plant: widget.plant,
                boxSizes: [30.0, 10.0, 25.0],
              ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../modules/models/entities/room.dart';
import '../../../utils/services/dialog_service.dart';
import '../../../utils/services/indicator_service.dart';
import 'components/room_lighting_level_indicator.dart';
import 'components/room_notifications_button.dart';
import 'components/room_temperature_indicator.dart';
import 'components/room_title.dart';
import 'components/room_water_level_indicator.dart';

class RoomCard extends StatefulWidget {
  final int index;
  final List<Room> rooms;

  RoomCard({
    required this.index,
    required this.rooms,
  });

  @override
  State<RoomCard> createState() => _RoomCardState();
}

class _RoomCardState extends State<RoomCard> {
  @override
  Widget build(BuildContext context) {
    final Room room = widget.rooms[widget.index];

    return GestureDetector(
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            25,
          ),
        ),
        margin: EdgeInsets.symmetric(
          vertical: 7,
          horizontal: 15,
        ).r,
        elevation: 5,
        child: Stack(
          alignment: Alignment.topRight,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 20.0,
              ).r,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  RoomTitle(name: room.roomName),
                  RoomTemperatureIndicator(temperature: room.temperature),
                  RoomLightingLevelIndicator(
                    lighting: generateLightingLevel(
                      room.lightingLevel,
                    ),
                  ),
                  RoomWaterLevelIndicator(
                    waterLevel: room.waterTank.waterLevel,
                  ),
                ],
              ),
            ),
            RoomNotificationsButton(
              roomName: room.roomName,
              notifications: room.notifications,
              onPressedFunction: () {
                showRoomNotifications(
                  context: context,
                  room: room,
                  onPressedFunction: () {
                    Navigator.pop(context);
                    setState(() {
                      room.notifications = [];
                    });
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

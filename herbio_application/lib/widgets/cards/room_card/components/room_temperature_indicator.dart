import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

class RoomTemperatureIndicator extends StatelessWidget {
  final double temperature;

  RoomTemperatureIndicator({required this.temperature});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "Temperature: $temperature°C",
          style: TextStyle(
            fontSize: 15.sp,
          ),
        ),
        SizedBox(
          height: 20.h,
        ),
      ],
    );
  }
}

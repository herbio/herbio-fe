import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

class RoomLightingLevelIndicator extends StatelessWidget {
  final List<Widget> lighting;

  RoomLightingLevelIndicator({required this.lighting});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "Lighting Level",
          style: TextStyle(
            fontSize: 15.0.sp,
          ),
        ),
        SizedBox(
          height: 10.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: lighting,
        ),
        SizedBox(
          height: 20.h,
        ),
      ],
    );
  }
}

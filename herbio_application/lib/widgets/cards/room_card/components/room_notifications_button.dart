import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../../../modules/models/notifications/herbio_notification.dart';

class RoomNotificationsButton extends StatelessWidget {
  final String roomName;
  final List<HerbioNotification> notifications;
  final void Function()? onPressedFunction;

  RoomNotificationsButton({
    required this.roomName,
    required this.notifications,
    required this.onPressedFunction,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (notifications.isEmpty)
          Padding(
            padding: const EdgeInsets.all(
              10.0,
            ).r,
            child: Tooltip(
              message: 'Show Room Notifications',
              waitDuration: const Duration(
                milliseconds: 500,
              ),
              child: ElevatedButton(
                child: SizedBox(
                  height: 60.h,
                  child: Icon(
                    Icons.notifications,
                    size: 25.sp,
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  shape: CircleBorder(),
                  padding: EdgeInsets.all(
                    0,
                  ).r,
                  primary: Theme.of(context).colorScheme.secondary,
                  onPrimary: Theme.of(context).primaryColorLight,
                ),
                onPressed: () {
                  if (!kIsWeb) {
                    Fluttertoast.showToast(
                      msg: "No notifications for $roomName.",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      textColor: Colors.white,
                      fontSize: 16.0.sp,
                    );
                  }
                },
              ),
            ),
          )
        else
          Padding(
            padding: const EdgeInsets.all(
              10.0,
            ).r,
            child: Stack(
              children: [
                Tooltip(
                  message: 'Show Room Notifications',
                  waitDuration: const Duration(
                    milliseconds: 500,
                  ),
                  child: ElevatedButton(
                    child: SizedBox(
                      height: 60.h,
                      child: Icon(
                        Icons.notifications,
                        size: 25.0.sp,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      shape: CircleBorder(),
                      padding: EdgeInsets.all(0),
                      primary: Theme.of(context).colorScheme.secondary,
                      onPrimary: Theme.of(context).primaryColorLight,
                    ),
                    onPressed: onPressedFunction,
                  ),
                ),
                Positioned(
                  // Draw Red Marble Notification Counter
                  top: 0.0,
                  right: 0.0,
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: 6,
                      vertical: 2,
                    ),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.red,
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      '${notifications.length}',
                      style: TextStyle(
                        color: Theme.of(context).primaryColorLight,
                        fontSize: 15.0.sp,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
      ],
    );
  }
}

import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

class RoomCycleTable extends StatelessWidget {
  final double shortestCycle;
  final double longestCycle;
  final double lastCycle;

  RoomCycleTable({
    required this.shortestCycle,
    required this.longestCycle,
    required this.lastCycle,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FittedBox(
          child: DataTable(
            columns: [
              DataColumn(
                label: Text(
                  'Shortest Cycle',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0.sp,
                  ),
                ),
              ),
              DataColumn(
                label: Text(
                  'Longest Cycle',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0.sp,
                  ),
                ),
              ),
              DataColumn(
                label: Text(
                  'Last Cycle',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15.0.sp,
                  ),
                ),
              ),
            ],
            rows: [
              DataRow(
                cells: [
                  DataCell(
                    Center(
                      child: Text(
                        '$shortestCycle days',
                        style: TextStyle(
                          fontSize: 15.0.sp,
                        ),
                      ),
                    ),
                  ),
                  DataCell(
                    Center(
                      child: Text(
                        '$longestCycle days',
                        style: TextStyle(
                          fontSize: 15.0.sp,
                        ),
                      ),
                    ),
                  ),
                  DataCell(
                    Center(
                      child: Text(
                        '$lastCycle days',
                        style: TextStyle(
                          fontSize: 15.0.sp,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        SizedBox(
          height: 15.h,
        ),
      ],
    );
  }
}

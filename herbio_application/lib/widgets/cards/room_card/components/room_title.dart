import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

class RoomTitle extends StatelessWidget {
  final String name;

  RoomTitle({required this.name});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(
            10.0,
          ).r,
          child: Text(
            "$name",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 25.0.sp,
            ),
          ),
        ),
        SizedBox(
          height: 10.h,
        ),
      ],
    );
  }
}

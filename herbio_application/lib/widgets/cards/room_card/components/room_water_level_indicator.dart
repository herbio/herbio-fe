import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

import '../../../../utils/services/indicator_service.dart';

class RoomWaterLevelIndicator extends StatelessWidget {
  final double waterLevel;

  RoomWaterLevelIndicator({required this.waterLevel});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          "Water Level",
          style: TextStyle(
            fontSize: 15.0.sp,
          ),
        ),
        SizedBox(
          height: 10.h,
        ),
        LinearPercentIndicator(
          animation: true,
          lineHeight: 25.0.sp,
          percent: waterLevel,
          center: Text(
            '${waterLevel * 100} %',
            style: TextStyle(
              fontSize: 15.0.sp,
            ),
          ),
          progressColor: getWaterPecentageColor(
            waterLevel,
          ),
        ),
        SizedBox(
          height: 20.h,
        ),
      ],
    );
  }
}

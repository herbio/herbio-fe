import 'package:flutter/material.dart';

import '../../utils/placeholder_data.dart';
import '../../utils/providers/lighting_requirements_provider.dart';
import '../../utils/services/indicator_service.dart';

class LightingRequirementsField extends StatefulWidget {
  final int lightingRequirement;
  final LightingRequirementsProvider lightingRequirementsProvider;

  LightingRequirementsField({
    required this.lightingRequirement,
    required this.lightingRequirementsProvider,
  });

  @override
  State<LightingRequirementsField> createState() =>
      _LightingRequirementsFieldState();
}

class _LightingRequirementsFieldState extends State<LightingRequirementsField> {
  @override
  Widget build(BuildContext context) {
    widget.lightingRequirementsProvider.setLightingRequirement(
      widget.lightingRequirement,
    );

    return StatefulBuilder(
      builder: (context, setState) {
        List<Widget> ligthingLevelIndicators = generateLightingLevel(
            widget.lightingRequirementsProvider.lightingRequirement);

        List<Widget> generateLightingLevelIndicatorField() {
          List<Widget> icons = [];

          ligthingLevelIndicators.asMap().forEach((index, value) {
            String key = lightingMapping.keys.firstWhere(
              (k) => lightingMapping[k]!['value'] == (index + 1),
              orElse: () => '',
            );

            icons.add(
              Tooltip(
                message: key != '' ? lightingMapping[key]!['range'] : '',
                child: GestureDetector(
                  child: value,
                  onTap: () {
                    setState(
                      () {
                        widget.lightingRequirementsProvider
                            .setLightingRequirement(index + 1);
                      },
                    );
                  },
                ),
              ),
            );
          });

          return icons;
        }

        return Column(
          children: [
            Text(
              'Lighting Requirement',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: generateLightingLevelIndicatorField(),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        );
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../../modules/models/types/field_type.dart';
import '../../utils/formatters/decimal_text_input_formatter.dart';
import '../../utils/providers/password_visibility_provider.dart';
import '../../utils/validators/field_validator.dart';
import '../buttons/password_visibility_switch_button.dart';

class RoundedTextField extends StatefulWidget {
  final String _label;
  final TextEditingController _controller;
  final PasswordVisibilityProvider _passwordVisibilityProvider;
  final FieldType _fieldType;
  final FieldValidator _fieldValidator;

  RoundedTextField(
      this._label, this._controller, this._passwordVisibilityProvider,
      [this._fieldType = FieldType.TEXT,
      this._fieldValidator = const FieldValidator()]);

  @override
  _RoundedTextFieldState createState() => _RoundedTextFieldState();
}

class _RoundedTextFieldState extends State<RoundedTextField> {
  late FocusNode _focusNode;

  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  void _requestFocus() {
    setState(() {
      FocusScope.of(context).requestFocus(_focusNode);
    });
  }

  @override
  Widget build(BuildContext context) {
    bool _passwordVisibility =
        Provider.of<PasswordVisibilityProvider>(context).isPasswordHidden;

    return Padding(
      padding: const EdgeInsets.all(
        15.0,
      ).r,
      child: Container(
        width: 300.w,
        child: TextFormField(
          autocorrect: false,
          controller: widget._controller,
          cursorColor: Theme.of(context).colorScheme.secondary,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(
                30.0,
              ),
            ),
            contentPadding: EdgeInsets.only(
              top: 15.0,
              bottom: 15.0,
              left: 30.0,
            ).r,
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(
                30.0,
              ),
              borderSide: BorderSide(
                color: Theme.of(context).colorScheme.secondary,
                width: 2.0,
              ),
            ),
            labelText: widget._label,
            labelStyle: TextStyle(
              color: _focusNode.hasFocus
                  ? Theme.of(context).colorScheme.secondary
                  : const Color(0xFF898989),
              fontSize: 15.sp,
            ),
            suffixIcon: widget._fieldType == FieldType.PASSWORD
                ? PasswordVisibilitySwitchButton(
                    _passwordVisibility,
                    widget._passwordVisibilityProvider.changePasswordVisibility,
                    _focusNode,
                  )
                : null,
          ),
          enableSuggestions: widget._fieldType != FieldType.PASSWORD,
          focusNode: _focusNode,
          inputFormatters: widget._fieldType == FieldType.NUMBER
              ? <TextInputFormatter>[
                  DecimalTextInputFormatter(decimalRange: 2),
                ]
              : null,
          keyboardType: widget._fieldType == FieldType.NUMBER
              ? TextInputType.numberWithOptions(
                  signed: false,
                  decimal: true,
                )
              : TextInputType.text,
          obscureText:
              widget._fieldType == FieldType.PASSWORD && _passwordVisibility,
          onSaved: (String? value) => widget._controller.text = value as String,
          onTap: _requestFocus,
          textAlign: TextAlign.left,
          validator: (value) {
            String? errorMessage = widget._fieldValidator.validate(
              value as String,
            );
            return errorMessage.isEmpty ? null : errorMessage;
          },
        ),
      ),
    );
  }
}

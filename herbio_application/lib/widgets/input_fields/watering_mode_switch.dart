import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../modules/models/types/field_type.dart';
import '../../modules/models/types/number_input_field_type.dart';
import '../../modules/models/types/watering_mode.dart';
import '../../utils/providers/password_visibility_provider.dart';
import '../../utils/providers/watering_mode_provider.dart';
import '../../utils/validators/general_field_validators/number_field_validator.dart';
import 'rounded_text_field.dart';

class WateringModeSwitch extends StatefulWidget {
  final WateringMode wateringMode;
  final WateringModeProvider wateringModeProvider;
  final TextEditingController lowerWaterThresholdController;
  final TextEditingController upperWaterThresholdController;
  final TextEditingController wateringAmountController;

  WateringModeSwitch({
    required this.wateringMode,
    required this.wateringModeProvider,
    required this.lowerWaterThresholdController,
    required this.upperWaterThresholdController,
    required this.wateringAmountController,
  });

  @override
  State<WateringModeSwitch> createState() => _WateringModeSwitchState();
}

class _WateringModeSwitchState extends State<WateringModeSwitch> {
  @override
  Widget build(BuildContext context) {
    PasswordVisibilityProvider passwordVisibilityProvider =
        Provider.of<PasswordVisibilityProvider>(context);

    widget.wateringModeProvider.setWateringMode(
      widget.wateringMode,
    );

    return StatefulBuilder(
      builder: (context, setState) {
        return Column(
          children: [
            Text(
              'Watering Mode',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ListTile(
                    title: const Text(
                      'Manual',
                    ),
                    leading: Radio<WateringMode>(
                      value: WateringMode.MANUAL,
                      groupValue: widget.wateringModeProvider.wateringMode,
                      fillColor: MaterialStateColor.resolveWith(
                        (states) => Theme.of(context).colorScheme.secondary,
                      ),
                      onChanged: (WateringMode? value) {
                        setState(
                          () {
                            widget.wateringModeProvider.setWateringMode(
                              value!,
                            );
                          },
                        );
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text(
                      'Automatic',
                    ),
                    leading: Radio<WateringMode>(
                      value: WateringMode.AUTO,
                      groupValue: widget.wateringModeProvider.wateringMode,
                      fillColor: MaterialStateColor.resolveWith(
                        (states) => Theme.of(context).colorScheme.secondary,
                      ),
                      onChanged: (WateringMode? value) {
                        setState(
                          () {
                            widget.wateringModeProvider.setWateringMode(
                              value!,
                            );
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            widget.wateringModeProvider.wateringMode == WateringMode.AUTO
                ? Column(
                    children: [
                      SizedBox(
                        height: 20,
                      ),
                      RoundedTextField(
                        'Lower Watering Threshold',
                        widget.lowerWaterThresholdController,
                        passwordVisibilityProvider,
                        FieldType.NUMBER,
                        NumberFieldValidator(
                          numberInputFieldType: NumberInputFieldType.DOUBLE,
                          min: 1,
                          max: 10,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      RoundedTextField(
                        'Upper Watering Threshold',
                        widget.upperWaterThresholdController,
                        passwordVisibilityProvider,
                        FieldType.NUMBER,
                        NumberFieldValidator(
                          numberInputFieldType: NumberInputFieldType.INTEGER,
                          min: 1,
                          max: 10,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      RoundedTextField(
                        'Water Amount (ml)',
                        widget.wateringAmountController,
                        passwordVisibilityProvider,
                        FieldType.NUMBER,
                        NumberFieldValidator(
                          numberInputFieldType: NumberInputFieldType.INTEGER,
                          min: 10,
                          max: 150,
                        ),
                      ),
                    ],
                  )
                : Container(),
          ],
        );
      },
    );
  }
}

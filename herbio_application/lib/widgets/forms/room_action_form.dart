import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:routemaster/routemaster.dart';

import '../../modules/models/entities/room.dart';
import '../../modules/models/types/field_type.dart';
import '../../modules/models/types/action_type.dart';
import '../../modules/models/types/number_input_field_type.dart';
import '../../utils/providers/password_visibility_provider.dart';
import '../../utils/services/http_request_services/room_request_service.dart';
import '../../utils/validators/general_field_validators/number_field_validator.dart';
import '../../utils/validators/general_field_validators/text_field_validator.dart';
import '../buttons/rounded_button.dart';
import '../input_fields/rounded_text_field.dart';

class RoomActionForm extends StatelessWidget {
  final Room room;
  final ActionType mode;
  final String buttonText;

  RoomActionForm({
    required this.room,
    required this.mode,
    required this.buttonText,
  });

  late final TextEditingController _roomNameController = TextEditingController(
    text: this.room.roomName,
  );
  late final TextEditingController _waterTankCapacityController =
      TextEditingController(
    text: this.room.waterTank.capacity.toString(),
  );

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void _submitForm(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      if (this.mode == ActionType.ADD) {
        createRoom(
          roomName: _roomNameController.text,
          waterTankCapacity: double.parse(
            _waterTankCapacityController.text,
          ),
        );
      } else if (this.mode == ActionType.EDIT) {
        print(_roomNameController.text);
        updateRoom(
          context: context,
          roomId: this.room.id,
          roomName: _roomNameController.text,
          waterTankCapacity: double.parse(
            _waterTankCapacityController.text,
          ),
        );
      }
      Navigator.of(context).pop();

      Routemaster.of(context).replace('/');
    }
  }

  @override
  Widget build(BuildContext context) {
    PasswordVisibilityProvider _passwordVisibilityProvider =
        Provider.of<PasswordVisibilityProvider>(context);

    return Form(
      key: _formKey,
      child: Column(
        children: [
          RoundedTextField(
            'Room Name',
            _roomNameController,
            _passwordVisibilityProvider,
            FieldType.TEXT,
            TextFieldValidator(),
          ),
          SizedBox(
            height: 20,
          ),
          RoundedTextField(
            'Water Tank Capacity',
            _waterTankCapacityController,
            _passwordVisibilityProvider,
            FieldType.NUMBER,
            NumberFieldValidator(
              numberInputFieldType: NumberInputFieldType.DOUBLE,
              min: 10,
              max: 1000000,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          RoundedButton(
            Text(buttonText),
            () {
              _submitForm(context);
            },
          ),
          SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}

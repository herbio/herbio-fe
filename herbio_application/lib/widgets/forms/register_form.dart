import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_pw_validator/flutter_pw_validator.dart';
import 'package:provider/provider.dart';

import '../../modules/models/types/field_type.dart';
import '../../utils/providers/password_visibility_provider.dart';
import '../../utils/services/http_request_services/authentication_request_service.dart';
import '../../utils/validators/registration_validators/register_email_confirm_validator.dart';
import '../../utils/validators/registration_validators/register_email_validator.dart';
import '../../utils/validators/registration_validators/register_password_confirm_validator.dart';
import '../buttons/rounded_button.dart';
import '../input_fields/rounded_text_field.dart';

class RegisterForm extends StatelessWidget {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _confirmEmailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void _submitForm(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      sendRegistrationRequest(
        context: context,
        username: _usernameController.text,
        email: _emailController.text,
        password: _passwordController.text,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    PasswordVisibilityProvider _passwordVisibilityProvider =
        Provider.of<PasswordVisibilityProvider>(context);

    RegisterPasswordConfirmValidator _registerPasswordConfirmValidator =
        RegisterPasswordConfirmValidator(_passwordController.text);

    RegisterEmailConfirmValidator _registerEmailConfirmValidator =
        RegisterEmailConfirmValidator("");

    return Form(
      key: _formKey,
      child: Column(
        children: [
          RoundedTextField(
            'Username',
            _usernameController,
            _passwordVisibilityProvider,
          ),
          RoundedTextField(
            'E-Mail',
            _emailController,
            _passwordVisibilityProvider,
            FieldType.EMAIL,
            RegisterEmailValidator(_registerEmailConfirmValidator),
          ),
          RoundedTextField(
            'Confirm E-Mail',
            _confirmEmailController,
            _passwordVisibilityProvider,
            FieldType.EMAIL,
            _registerEmailConfirmValidator,
          ),
          RoundedTextField(
            'Password',
            _passwordController,
            _passwordVisibilityProvider,
            FieldType.PASSWORD,
          ),
          Padding(
            padding: const EdgeInsets.only(
              bottom: 10.0,
            ).r,
            child: FlutterPwValidator(
              controller: _passwordController,
              minLength: 10,
              uppercaseCharCount: 3,
              numericCharCount: 2,
              specialCharCount: 2,
              width: 300.w,
              height: 85.h,
              onSuccess: () {
                _registerPasswordConfirmValidator
                    .password(_passwordController.text);
              },
              onFail: () {},
            ),
          ),
          SizedBox(
            height: 10.h,
          ),
          RoundedTextField(
            'Confirm Password',
            _confirmPasswordController,
            _passwordVisibilityProvider,
            FieldType.PASSWORD,
            _registerPasswordConfirmValidator,
          ),
          SizedBox(
            height: 20.h,
          ),
          RoundedButton(
            Text(
              'Register',
              style: TextStyle(
                fontSize: 15.sp,
              ),
            ),
            () {
              _submitForm(context);
            },
          ),
          SizedBox(
            height: 10.h,
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:routemaster/routemaster.dart';

import '../../../modules/models/entities/plant.dart';
import '../../../modules/models/types/action_type.dart';
import '../../../utils/placeholder_data.dart';
import '../../../utils/providers/lighting_requirements_provider.dart';
import '../../../utils/providers/watering_mode_provider.dart';
import '../../../utils/services/http_request_services/plant_request_service.dart';
import 'layouts/android_plant_action_form_layout.dart';
import 'layouts/web_plant_action_form_layout.dart';

// ignore: must_be_immutable
class PlantActionForm extends StatelessWidget {
  final int roomId;
  final Plant plant;
  final WateringModeProvider wateringModeProvider;
  final LightingRequirementsProvider lightingRequirementsProvider;
  final ActionType mode;
  final String buttonText;

  PlantActionForm({
    required this.roomId,
    required this.plant,
    required this.lightingRequirementsProvider,
    required this.wateringModeProvider,
    required this.mode,
    required this.buttonText,
  });

  late final TextEditingController _plantNameController = TextEditingController(
    text: this.plant.plantName,
  );

  late final TextEditingController _wateringRequirementController =
      TextEditingController(
    text: this.plant.wateringRequirement.toString(),
  );

  late final TextEditingController _lowerWaterThresholdController =
      TextEditingController(
    text: this.plant.lowerWaterThreshold.toString(),
  );

  late final TextEditingController _upperWaterThresholdController =
      TextEditingController(
    text: this.plant.upperWaterThreshold.toString(),
  );

  late final TextEditingController _wateringAmountController =
      TextEditingController(
    text: this.plant.wateringAmount.toString(),
  );

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void _submitForm(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      if (this.mode == ActionType.ADD) {
        createPlant(
          roomId: roomId,
          plantName: _plantNameController.text,
          lightingRequirement: lightingMappingReversed[
              lightingRequirementsProvider.lightingRequirement]!['value']!,
          waterRequirement: int.parse(
            _wateringRequirementController.text,
          ),
          wateringMode:
              wateringModeProvider.wateringMode.toString().split('.')[1],
          wateringAmount: int.parse(
            _wateringAmountController.text,
          ),
          lowerWaterThreshold: double.parse(
            _lowerWaterThresholdController.text,
          ),
          upperWaterThreshold: double.parse(
            _upperWaterThresholdController.text,
          ),
        );
      } else if (this.mode == ActionType.EDIT) {
        updatePlant(
          plantId: this.plant.id,
          roomId: roomId,
          plantName: _plantNameController.text,
          lightingRequirement: lightingMappingReversed[
              lightingRequirementsProvider.lightingRequirement]!['value']!,
          waterRequirement: int.parse(
            _wateringRequirementController.text,
          ),
          wateringMode:
              wateringModeProvider.wateringMode.toString().split('.')[1],
          wateringAmount: int.parse(
            _wateringAmountController.text,
          ),
          lowerWaterThreshold: double.parse(
            _lowerWaterThresholdController.text,
          ),
          upperWaterThreshold: double.parse(
            _upperWaterThresholdController.text,
          ),
        );
      }

      Navigator.of(context).pop();

      Routemaster.of(context).replace(
        kIsWeb ? '/rooms' : '/',
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return kIsWeb
        ? WebPlantActionFormLayout(
            formKey: _formKey,
            plant: plant,
            plantNameController: _plantNameController,
            wateringRequirementController: _wateringRequirementController,
            lowerWaterThresholdController: _lowerWaterThresholdController,
            upperWaterThresholdController: _upperWaterThresholdController,
            wateringAmountController: _wateringAmountController,
            mode: mode,
            lightingRequirementsProvider: lightingRequirementsProvider,
            wateringModeProvider: wateringModeProvider,
            buttonText: buttonText,
            submitFormFunction: () {
              _submitForm(context);
            },
          )
        : AndroidPlantActionFormLayout(
            formKey: _formKey,
            plant: plant,
            plantNameController: _plantNameController,
            wateringRequirementController: _wateringRequirementController,
            lowerWaterThresholdController: _lowerWaterThresholdController,
            upperWaterThresholdController: _upperWaterThresholdController,
            wateringAmountController: _wateringAmountController,
            mode: mode,
            lightingRequirementsProvider: lightingRequirementsProvider,
            wateringModeProvider: wateringModeProvider,
            buttonText: buttonText,
            submitFormFunction: () {
              _submitForm(context);
            },
          );
  }
}

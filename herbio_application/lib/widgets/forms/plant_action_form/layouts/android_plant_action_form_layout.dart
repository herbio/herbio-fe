import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../../../../modules/models/entities/plant.dart';
import '../../../../modules/models/types/action_type.dart';
import '../../../../modules/models/types/field_type.dart';
import '../../../../modules/models/types/number_input_field_type.dart';
import '../../../../utils/providers/lighting_requirements_provider.dart';
import '../../../../utils/providers/password_visibility_provider.dart';
import '../../../../utils/providers/watering_mode_provider.dart';
import '../../../../utils/validators/general_field_validators/number_field_validator.dart';
import '../../../../utils/validators/general_field_validators/text_field_validator.dart';
import '../../../buttons/rounded_button.dart';
import '../../../input_fields/lighting_requirements_field.dart';
import '../../../input_fields/rounded_text_field.dart';
import '../../../input_fields/watering_mode_switch.dart';

class AndroidPlantActionFormLayout extends StatelessWidget {
  final Plant plant;
  final GlobalKey<FormState> formKey;
  final TextEditingController plantNameController;
  final TextEditingController wateringRequirementController;
  final TextEditingController lowerWaterThresholdController;
  final TextEditingController upperWaterThresholdController;
  final TextEditingController wateringAmountController;
  final ActionType mode;
  final LightingRequirementsProvider lightingRequirementsProvider;
  final WateringModeProvider wateringModeProvider;
  final String buttonText;
  final Function submitFormFunction;

  AndroidPlantActionFormLayout({
    required this.plant,
    required this.formKey,
    required this.plantNameController,
    required this.wateringRequirementController,
    required this.lowerWaterThresholdController,
    required this.upperWaterThresholdController,
    required this.wateringAmountController,
    required this.mode,
    required this.lightingRequirementsProvider,
    required this.wateringModeProvider,
    required this.buttonText,
    required this.submitFormFunction,
  });

  @override
  Widget build(BuildContext context) {
    PasswordVisibilityProvider passwordVisibilityProvider =
        Provider.of<PasswordVisibilityProvider>(context);

    return Form(
      key: formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 10.0,
        ),
        child: Column(
          children: [
            // ImagePickerField(
            //   imagePath: imagePath,
            //   image: imagePathProvider.image,
            //   imagePathProvider: imagePathProvider,
            // ),
            // SizedBox(
            //   width: 30,
            // ),
            RoundedTextField(
              'Plant Name',
              plantNameController,
              passwordVisibilityProvider,
              FieldType.TEXT,
              TextFieldValidator(),
            ),
            SizedBox(
              height: 20,
            ),
            RoundedTextField(
              'Water Requirement (ml)',
              wateringRequirementController,
              passwordVisibilityProvider,
              FieldType.NUMBER,
              NumberFieldValidator(
                numberInputFieldType: NumberInputFieldType.INTEGER,
                min: 10,
                max: 1000,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            LightingRequirementsField(
              lightingRequirement: this.mode == ActionType.ADD
                  ? 0
                  : this.plant.lightingRequirement,
              lightingRequirementsProvider: this.lightingRequirementsProvider,
            ),
            SizedBox(
              height: 20,
            ),
            WateringModeSwitch(
              wateringMode: this.plant.wateringMode,
              wateringModeProvider: this.wateringModeProvider,
              lowerWaterThresholdController: this.lowerWaterThresholdController,
              upperWaterThresholdController: this.upperWaterThresholdController,
              wateringAmountController: this.wateringAmountController,
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RoundedButton(
                  Text(buttonText),
                  submitFormFunction,
                ),
                SizedBox(
                  height: 10,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

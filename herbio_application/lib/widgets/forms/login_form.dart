import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../../modules/models/types/field_type.dart';
import '../../utils/providers/password_visibility_provider.dart';
import '../../utils/services/http_request_services/authentication_request_service.dart';
import '../../utils/validators/general_field_validators/text_field_validator.dart';
import '../buttons/rounded_button.dart';
import '../input_fields/rounded_text_field.dart';

class LoginForm extends StatelessWidget {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  final String redirectPath;

  LoginForm({
    required this.redirectPath,
  });

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  void _submitForm(BuildContext context, String redirectPath) async {
    if (_formKey.currentState!.validate()) {
      sendSignInRequest(
        context: context,
        username: _usernameController.text,
        password: _passwordController.text,
        redirectPath: redirectPath,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    PasswordVisibilityProvider _passwordVisibilityProvider =
        Provider.of<PasswordVisibilityProvider>(context);

    return Form(
      key: _formKey,
      child: Column(
        children: [
          RoundedTextField(
            'Username',
            _usernameController,
            _passwordVisibilityProvider,
            FieldType.TEXT,
            TextFieldValidator(),
          ),
          RoundedTextField(
            'Password',
            _passwordController,
            _passwordVisibilityProvider,
            FieldType.PASSWORD,
            TextFieldValidator(),
          ),
          SizedBox(
            height: 20.h,
          ),
          RoundedButton(
            Text(
              'Sign in',
              style: TextStyle(
                fontSize: 15.sp,
              ),
            ),
            () {
              _submitForm(
                context,
                this.redirectPath,
              );
            },
          ),
        ],
      ),
    );
  }
}

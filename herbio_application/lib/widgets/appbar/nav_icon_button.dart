import 'dart:html';
import 'dart:js' as js;
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NavIconButton extends StatefulWidget {
  @override
  _NavIconButtonState createState() => _NavIconButtonState();
}

class _NavIconButtonState extends State<NavIconButton> {
  Color _textColor = Colors.white;
  int _enterCounter = 0;
  int _exitCounter = 0;
  double x = 0.0;
  double y = 0.0;

  void _incrementEnter(PointerEvent details) {
    setState(
      () {
        _enterCounter++;
      },
    );
  }

  void _incrementExit(PointerEvent details) {
    setState(
      () {
        _textColor = Theme.of(context).primaryColorLight;
        _exitCounter++;
      },
    );
  }

  void _updateLocation(PointerEvent details) {
    setState(
      () {
        _textColor = Theme.of(context).accentColor;
        x = details.position.dx;
        y = details.position.dy;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: _incrementEnter,
      onHover: _updateLocation,
      onExit: _incrementExit,
      child: IconButton(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        icon: SvgPicture.asset(
          "../assets/icons/gitlab.svg",
          color: _textColor,
        ),
        onPressed: () {
          js.context.callMethod(
            'open',
            ['${'https://gitlab.com/herbio/herbio-fe/-/tree/main/herbio_application/'}'],
          );
        },
      ),
    );
  }
}

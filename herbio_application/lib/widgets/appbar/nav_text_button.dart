import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:herbio_application/providers/layout_provider.dart';

class NavTextButton extends StatefulWidget {
  final String text;

  const NavTextButton({Key? key, required this.text}) : super(key: key);

  @override
  _NavTextButtonState createState() => _NavTextButtonState();
}

class _NavTextButtonState extends State<NavTextButton> {
  Color _textColor = Colors.white;
  int _enterCounter = 0;
  int _exitCounter = 0;
  double x = 0.0;
  double y = 0.0;

  void _incrementEnter(PointerEvent details) {
    setState(
      () {
        _enterCounter++;
      },
    );
  }

  void _incrementExit(PointerEvent details) {
    setState(
      () {
        _textColor = Theme.of(context).primaryColorLight;
        _exitCounter++;
      },
    );
  }

  void _updateLocation(PointerEvent details) {
    setState(
      () {
        _textColor = Theme.of(context).accentColor;
        x = details.position.dx;
        y = details.position.dy;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    LayoutProvider layoutProvider = Provider.of<LayoutProvider>(context);

    return MouseRegion(
      onEnter: _incrementEnter,
      onHover: _updateLocation,
      onExit: _incrementExit,
      child: TextButton(
        style: ButtonStyle(
          overlayColor: MaterialStateColor.resolveWith(
            (states) => Colors.transparent,
          ),
        ),
        child: Text(
          this.widget.text,
          style: TextStyle(
            fontSize: 16,
            color: _textColor,
          ),
        ),
        onPressed: () {
          layoutProvider.changeCurrentLayout(
            this.widget.text.toLowerCase().split(' ').join('_'),
          );
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:routemaster/routemaster.dart';

import '../../../constants/asset_constants.dart';
import '../../../modules/screens/logged_in_screens/rooms_screen.dart';
import '../../../modules/screens/logged_in_screens/statistics_screen.dart';
import '../../../utils/services/http_request_services/authentication_request_service.dart';
import '../../buttons/appbar/home_button.dart';
import '../../buttons/appbar/nav_icon_button.dart';
import '../../buttons/appbar/nav_text_button.dart';

class HerbIOLoggedInAppBar extends StatefulWidget
    implements PreferredSizeWidget {
  HerbIOLoggedInAppBar({Key? key})
      : preferredSize = Size.fromHeight(80.h),
        super(key: key);

  @override
  final Size preferredSize;

  @override
  _HerbIOLoggedInAppBarState createState() => _HerbIOLoggedInAppBarState();
}

class _HerbIOLoggedInAppBarState extends State<HerbIOLoggedInAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      toolbarHeight: 100.h,
      leadingWidth: 100.w,
      backgroundColor: Theme.of(context).primaryColor,
      leading: IconButton(
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        icon: SvgPicture.asset(
          HERBIO_ICON,
          color: Theme.of(context).primaryColorLight,
        ),
        onPressed: () {
          Routemaster.of(context).replace('/rooms');
        },
      ),
      title: HomeButton(
        path: '/rooms',
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.all(
            10.0,
          ).r,
          child: NavTextButton(
            text: 'Rooms',
            screen: RoomsScreen(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(
            10.0,
          ).r,
          child: NavTextButton(
            text: 'Statistics',
            screen: StatisticsScreen(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(
            10.0,
          ).r,
          child: NavIconButton(
            'Logout',
            Theme.of(context).primaryColorLight,
            Theme.of(context).colorScheme.secondary,
            Icons.logout,
            () {
              logout(context);
            },
          ),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../buttons/appbar/home_button.dart';

class HerbIOMobileAppBar extends StatefulWidget implements PreferredSizeWidget {
  HerbIOMobileAppBar({Key? key})
      : preferredSize = Size.fromHeight(60.h),
        super(key: key);

  @override
  final Size preferredSize;

  @override
  _HerbIOMobileAppBarState createState() => _HerbIOMobileAppBarState();
}

class _HerbIOMobileAppBarState extends State<HerbIOMobileAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      iconTheme: IconThemeData(
        color: Theme.of(context).colorScheme.secondary,
      ),
      backgroundColor: Theme.of(context).primaryColor,
      title: HomeButton(
        path: '/',
      ),
    );
  }
}

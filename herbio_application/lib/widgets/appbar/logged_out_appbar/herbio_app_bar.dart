import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:routemaster/routemaster.dart';

import '../../../constants/asset_constants.dart';
import '../../../modules/screens/logged_out_screens/about_us_screen.dart';
import '../../../modules/screens/logged_out_screens/register_screen.dart';
import '../../../modules/screens/logged_out_screens/sign_in_screen.dart';
import '../../buttons/appbar/home_button.dart';
import '../../buttons/appbar/nav_icon_button.dart';
import '../../buttons/appbar/nav_text_button.dart';

class HerbIOAppBar extends StatefulWidget implements PreferredSizeWidget {
  HerbIOAppBar({Key? key})
      : preferredSize = Size.fromHeight(80.h),
        super(key: key);

  @override
  final Size preferredSize;

  @override
  _HerbIOAppbarState createState() => _HerbIOAppbarState();
}

class _HerbIOAppbarState extends State<HerbIOAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      toolbarHeight: 100.h,
      leadingWidth: 100.w,
      backgroundColor: Theme.of(context).primaryColor,
      leading: IconButton(
        highlightColor: Colors.transparent,
        icon: SvgPicture.asset(
          HERBIO_ICON,
          color: Theme.of(context).primaryColorLight,
        ),
        onPressed: () {
          Routemaster.of(context).replace('/');
        },
        splashColor: Colors.transparent,
      ),
      title: HomeButton(
        path: '/',
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.all(
            10.0,
          ).r,
          child: NavTextButton(
            text: 'About Us',
            screen: AboutUsScreen(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(
            10.0,
          ).r,
          child: NavTextButton(
            text: 'Sign In',
            screen: SignInScreen(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(
            10.0,
          ).r,
          child: NavTextButton(
            text: 'Register',
            screen: RegisterScreen(),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(
            10.0,
          ).r,
          child: Container(
            height: 50.h,
            width: 50.w,
            child: NavIconButton(
              'View Source Code',
              Theme.of(context).primaryColorLight,
              Theme.of(context).colorScheme.secondary,
            ),
          ),
        ),
      ],
    );
  }
}

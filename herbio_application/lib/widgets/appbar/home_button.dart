import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:herbio_application/providers/layout_provider.dart';

class HomeButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    LayoutProvider layoutProvider = Provider.of<LayoutProvider>(context);

    return TextButton(
      style: ButtonStyle(
        overlayColor: MaterialStateColor.resolveWith(
          (states) => Colors.transparent,
        ),
      ),
      child: RichText(
        text: TextSpan(
          text: "Herb",
          style: TextStyle(
            fontSize: 28,
            color: Theme.of(context).primaryColorLight,
          ),
          children: <TextSpan>[
            TextSpan(
              text: "IO",
              style: TextStyle(
                fontSize: 28,
                color: Theme.of(context).accentColor,
              ),
            ),
          ],
        ),
      ),
      onPressed: () {
        layoutProvider.changeCurrentLayout(
          "home",
        );
      },
    );
  }
}

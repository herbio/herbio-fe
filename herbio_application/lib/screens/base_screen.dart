import 'package:flutter/material.dart';
import 'package:herbio_application/layouts/register_layout.dart';
import 'package:herbio_application/layouts/sign_in_layout.dart';
import 'package:provider/provider.dart';
import 'package:herbio_application/layouts/home_layout.dart';
import 'package:herbio_application/layouts/about_us_layout.dart';
import 'package:herbio_application/providers/layout_provider.dart';
import 'package:herbio_application/widgets/appbar/home_button.dart';
import 'package:herbio_application/widgets/appbar/nav_icon_button.dart';
import 'package:herbio_application/widgets/appbar/nav_text_button.dart';

class BaseScreen extends StatelessWidget {
  final LayoutProvider _layoutProvider = LayoutProvider();

  @override
  Widget build(BuildContext context) {
    LayoutProvider layoutProvider = Provider.of<LayoutProvider>(context);

    return Scaffold(
      appBar: AppBar(
        leading: Image(
            image: AssetImage(
              'icons/HerbIO_Logo2.png',
            ),

          ),
        toolbarHeight: 90,
        leadingWidth: 120,
        title: HomeButton(),
        actions: [
          Padding(
            padding: const EdgeInsets.all(
              10.0,
            ),
            child: NavTextButton(
              text: 'About Us',
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(
              10.0,
            ),
            child: NavTextButton(
              text: 'Sign In',
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(
              10.0,
            ),
            child: NavTextButton(
              text: 'Register',
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(
              10.0,
            ),
            child: NavIconButton(),
          ),
        ],
      ),
      body: layoutProvider.currentLayout == 'home'
          ? HomeLayout()
          : layoutProvider.currentLayout == 'about_us'
              ? AboutUsLayout()
              : layoutProvider.currentLayout == 'sign_in'
                  ? SignInLayout()
                  : layoutProvider.currentLayout == 'register'
                      ? RegisterLayout()
                      : Container(),
    );
  }
}

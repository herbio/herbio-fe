import '../../../modules/models/types/number_input_field_type.dart';
import '../field_validator.dart';

class NumberFieldValidator implements FieldValidator {
  final NumberInputFieldType numberInputFieldType;
  final double min;
  final double max;

  NumberFieldValidator({
    required this.numberInputFieldType,
    required this.min,
    required this.max,
  });

  @override
  String validate(String input) {
    String errorMessage = '';

    num numberInput = numberInputFieldType == NumberInputFieldType.INTEGER
        ? double.parse(input).floor()
        : double.parse(input);

    if (input == '') {
      errorMessage = 'Unfilled field.';
    } else {
      if (numberInput < min || numberInput > max) {
        errorMessage = 'Value must be in range ($min, $max).';
      }
    }

    return errorMessage;
  }
}

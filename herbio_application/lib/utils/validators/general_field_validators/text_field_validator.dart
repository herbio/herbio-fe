import '../field_validator.dart';

class TextFieldValidator implements FieldValidator {
  TextFieldValidator();

  @override
  String validate(String input) {
    return input == '' ? 'Unfilled field' : '';
  }
}

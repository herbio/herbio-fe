import '../field_validator.dart';
import 'register_email_confirm_validator.dart';

class RegisterEmailValidator implements FieldValidator {
  RegisterEmailConfirmValidator _registerEmailConfirmValidator;

  RegisterEmailValidator(this._registerEmailConfirmValidator);

  @override
  String validate(String input) {
    this._registerEmailConfirmValidator.email(input);

    return RegExp(
      r'[a-z0-9-_.]+@stuba\.sk',
    ).hasMatch(input)
        ? ''
        : 'Unsupported E-mail provider';
  }
}

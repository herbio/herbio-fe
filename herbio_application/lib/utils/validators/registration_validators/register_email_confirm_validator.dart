import '../field_validator.dart';

class RegisterEmailConfirmValidator implements FieldValidator {
  String _email;

  RegisterEmailConfirmValidator(this._email);

  void email(String text) {
    _email = text;
  }

  @override
  String validate(String input) {
    return (_email.isNotEmpty && input.isNotEmpty && _email == input)
        ? ''
        : 'E-mail addresses are not matching';
  }
}

import '../field_validator.dart';

class RegisterPasswordConfirmValidator implements FieldValidator {
  String _password;

  RegisterPasswordConfirmValidator(this._password);

  void password(String text) {
    _password = text;
  }

  @override
  String validate(String input) {
    return (_password.isNotEmpty && input.isNotEmpty && _password == input)
        ? ''
        : 'Passwords are not matching';
  }
}

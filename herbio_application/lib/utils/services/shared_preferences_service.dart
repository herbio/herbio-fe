import 'package:shared_preferences/shared_preferences.dart';

import 'http_request_services/authentication_request_service.dart';

Future<String?> getToken() async {
  final prefs = await SharedPreferences.getInstance();

  if (DateTime.now().isAfter(DateTime.parse(prefs.getString('expiresAt')!))) {
    refreshJwtToken(prefs);
  }

  return prefs.getString('jwtToken');
}

Future<List<String>?> getCredentials(SharedPreferences prefs) async {
  return [prefs.getString('refreshToken')!, prefs.getString('username')!];
}

Future<List<String>?> getRoles() async {
  final prefs = await SharedPreferences.getInstance();

  return prefs.getStringList('roles');
}

void saveDataToSharedPref(dynamic data) async {
  final prefs = await SharedPreferences.getInstance();

  List<String> _keys = [
    'id',
    'username',
    'email',
    'jwtToken',
    'type',
    'expiresAt',
    'refreshToken',
  ];

  _keys.forEach(
    (key) async {
      await prefs.setString(key, data[key]);
    },
  );

  await prefs.setStringList('roles', data['roles'].cast<String>());
}

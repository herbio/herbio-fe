import 'package:flutter/material.dart';

import '../../modules/models/entities/room.dart';
import '../../modules/models/notifications/herbio_notification.dart';

List<Widget> generateNotifications(Room room) {
  List<Widget> notifications = [];

  room.notifications.forEach(
    (HerbioNotification notification) => notifications.add(
      Padding(
        padding: const EdgeInsets.only(bottom: 10.0),
        child: Row(
          children: [
            notification.icon,
            SizedBox(
              width: 10,
            ),
            Flexible(
              child: Text(
                notification.notificationText,
              ),
            ),
          ],
        ),
      ),
    ),
  );

  return notifications;
}

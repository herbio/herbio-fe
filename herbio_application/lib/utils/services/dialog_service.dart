import 'package:flutter/material.dart';

import '../../modules/models/entities/plant.dart';
import '../../modules/models/entities/room.dart';
import '../../modules/models/types/action_type.dart';
import '../../widgets/forms/plant_action_form/plant_action_form.dart';
import '../../widgets/forms/room_action_form.dart';
import '../providers/lighting_requirements_provider.dart';
import '../providers/watering_mode_provider.dart';
import 'notification_service.dart';

void showRoomNotifications({
  required BuildContext context,
  required Room room,
  required void Function()? onPressedFunction,
}) {
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(
                25.0,
              ),
            ),
          ),
          title: Center(
            child: Text(
              'Notifications for ${room.roomName}',
            ),
          ),
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: generateNotifications(room),
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: onPressedFunction,
              child: Text(
                'Mark as Read',
                style: TextStyle(
                  color: Theme.of(context).colorScheme.secondary,
                ),
              ),
            )
          ],
        );
      });
}

void roomActionsDialog({
  required BuildContext context,
  required String title,
  required Room room,
  required ActionType mode,
}) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(
              25.0,
            ),
          ),
        ),
        title: Center(
          child: Text(
            title,
          ),
        ),
        content: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              RoomActionForm(
                room: room,
                mode: mode,
                buttonText: mode == ActionType.ADD ? 'Add Room' : 'Edit Room',
              ),
            ],
          ),
        ),
      );
    },
  );
}

void removeDialog({
  required BuildContext context,
  required String title,
  required String target,
  required void Function()? onPressedFunction,
}) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(
              25.0,
            ),
          ),
        ),
        title: Center(
          child: Text(
            title,
          ),
        ),
        content: Text(
          'Are you sure to remove $target ?',
        ),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Cancel',
              style: TextStyle(
                color: Theme.of(context).colorScheme.secondary,
              ),
            ),
          ),
          TextButton(
            onPressed: onPressedFunction,
            child: Text(
              'Remove',
              style: TextStyle(
                color: Theme.of(context).colorScheme.secondary,
              ),
            ),
          ),
        ],
      );
    },
  );
}

void plantActionsDialog({
  required BuildContext context,
  required String title,
  required ActionType mode,
  required Plant plant,
  required int roomId,
  required LightingRequirementsProvider lightingRequirementsProvider,
  required WateringModeProvider wateringModeProvider,
}) {
  showDialog(
    context: context,
    builder: (context) {
      return StatefulBuilder(
        builder: (context, setState) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(
                  25.0,
                ),
              ),
            ),
            title: Center(
              child: Text(
                title,
              ),
            ),
            content: Container(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    PlantActionForm(
                      roomId: roomId,
                      plant: plant,
                      mode: mode,
                      buttonText:
                          mode == ActionType.ADD ? 'Add Plant' : 'Edit Plant',
                      lightingRequirementsProvider:
                          lightingRequirementsProvider,
                      wateringModeProvider: wateringModeProvider,
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      );
    },
  );
}

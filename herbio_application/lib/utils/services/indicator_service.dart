import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

List<Widget> generateLightingLevel(int lightingLevel) {
  List<Widget> lightingLevelIndicators = [];

  for (int i = 0; i < lightingLevel; i++) {
    lightingLevelIndicators.add(
      Padding(
        padding: const EdgeInsets.all(3.0),
        child: Icon(
          Icons.wb_sunny,
          size: kIsWeb ? 40.sp : 35.sp,
          color: Colors.orange,
        ),
      ),
    );
  }

  for (int i = 0; i < 5 - lightingLevel; i++) {
    lightingLevelIndicators.add(
      Padding(
        padding: const EdgeInsets.all(3.0),
        child: Icon(
          Icons.wb_sunny,
          size: kIsWeb ? 40.sp : 35.sp,
        ),
      ),
    );
  }

  return lightingLevelIndicators;
}

Color getWaterPecentageColor(double percentage) {
  Color color = Colors.white;

  if (percentage >= 0.9) {
    color = Colors.blue;
  } else if (0.75 <= percentage && percentage < 0.9) {
    color = Colors.green;
  } else if (0.5 <= percentage && percentage < 0.75) {
    color = Colors.yellow;
  } else if (0.25 <= percentage && percentage < 0.50) {
    color = Colors.orange;
  } else {
    color = Colors.red;
  }

  return color;
}

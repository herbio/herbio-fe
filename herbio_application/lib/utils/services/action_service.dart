import 'package:flutter/material.dart';

import '../../modules/models/entities/plant.dart';
import '../../modules/models/entities/room.dart';
import '../../modules/models/types/action_type.dart';
import '../providers/lighting_requirements_provider.dart';
import '../providers/watering_mode_provider.dart';
import 'dialog_service.dart';

void executeRoomAction({
  required BuildContext context,
  required String title,
  required Room room,
  required ActionType mode,
}) {
  roomActionsDialog(
    context: context,
    title: title,
    room: room,
    mode: mode,
  );
}

void executePlantAction({
  required BuildContext context,
  required String title,
  required ActionType mode,
  required Plant plant,
  required int roomId,
  required LightingRequirementsProvider lightingRequirementsProvider,
  required WateringModeProvider wateringModeProvider,
}) {
  plantActionsDialog(
    context: context,
    title: title,
    mode: mode,
    plant: plant,
    roomId: roomId,
    lightingRequirementsProvider: lightingRequirementsProvider,
    wateringModeProvider: wateringModeProvider,
  );
}

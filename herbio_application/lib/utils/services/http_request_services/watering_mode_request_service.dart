import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../constants/api_app_paths.dart';
import '../shared_preferences_service.dart';

void updateWateringMode({
  required int wateringModeId,
  required String wateringMode,
  required int wateringAmount,
  required double lowerWaterThreshold,
  required double upperWaterThreshold,
}) async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  try {
    var response = await client.put(
      Uri.parse('$WATERING_PROFILES/$wateringModeId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${_jwtToken!}'
      },
      body: jsonEncode(
        <String, dynamic>{
          'humidityThresholdDown': lowerWaterThreshold,
          'humidityThresholdUp': upperWaterThreshold,
          'wateringAmount': wateringAmount,
          'wateringMode': wateringMode,
        },
      ),
    );

    if (response.statusCode >= 300) {
      print(
        'UpdateWateringMode - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }
}

Future<Map<String, dynamic>?> getWateringMode({
  required int plantId,
}) async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  Map<String, dynamic> wateringMode = {};

  try {
    var response = await client.get(
      Uri.parse('$WATERING_PROFILES/plant/$plantId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${_jwtToken!}'
      },
    );

    if (response.statusCode >= 300) {
      print(
        'GetWateringMode - Something went wrong, server returned: ${response.statusCode}',
      );
    } else {
      wateringMode = jsonDecode(response.body);
    }
  } finally {
    client.close();
  }

  return wateringMode;
}

Future<void> createWateringMode({
  required int plantId,
  required String wateringMode,
  required int wateringAmount,
  required double lowerWaterThreshold,
  required double upperWaterThreshold,
}) async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  try {
    var response = await client.post(
      Uri.parse('$WATERING_PROFILES/plant/$plantId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${_jwtToken!}'
      },
      body: jsonEncode(
        <String, dynamic>{
          'humidityThresholdDown': lowerWaterThreshold,
          'humidityThresholdUp': upperWaterThreshold,
          'wateringAmount': wateringAmount,
          'wateringMode': wateringMode,
        },
      ),
    );

    if (response.statusCode >= 300) {
      print(
        'CreateWateringMode - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }
}

import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:routemaster/routemaster.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../constants/api_auth_paths.dart';
import '../shared_preferences_service.dart';

void sendSignInRequest({
  required BuildContext context,
  required String username,
  required String password,
  required String redirectPath,
}) async {
  var client = http.Client();

  try {
    var response = await client.post(
      Uri.parse(LOGIN),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'password': password,
        'username': username,
      }),
    );

    if (response.statusCode == 200) {
      saveDataToSharedPref(jsonDecode(response.body));
      Routemaster.of(context).replace(redirectPath);
    } else {
      print(
        'SignIn - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }
}

void sendLogoutRequest({
  required BuildContext context,
}) async {
  final prefs = await SharedPreferences.getInstance();

  List<String>? _credentials = await getCredentials(prefs);

  var client = http.Client();

  try {
    var response = await client.post(
      Uri.parse(LOGOUT),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          'refreshToken': _credentials![0],
          'username': _credentials[1],
        },
      ),
    );

    if (response.statusCode == 200) {
      prefs.clear();

      Routemaster.of(context).replace('/sign-in');
    } else {
      print(
        'Logout - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }
}

void logout(
  BuildContext context,
) async {
  sendLogoutRequest(context: context);
}

void refreshJwtToken(
  SharedPreferences prefs,
) async {
  List<String>? _credentials = await getCredentials(prefs);

  var client = http.Client();

  try {
    var response = await client.post(
      Uri.parse(REFRESH),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(
        <String, String>{
          'refreshToken': _credentials![0],
          'username': _credentials[1],
        },
      ),
    );

    if (response.statusCode >= 300) {
      print(
        'RefreshJwtToken - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }
}

void sendRegistrationRequest({
  required BuildContext context,
  required String username,
  required String email,
  required String password,
}) async {
  var client = http.Client();

  try {
    var response = await client.post(
      Uri.parse(SIGNUP),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'email': email,
        'password': password,
        'username': username,
      }),
    );

    if (response.statusCode == 200) {
      Routemaster.of(context).push('/sign-in');
    } else {
      print(
        'Register - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }
}

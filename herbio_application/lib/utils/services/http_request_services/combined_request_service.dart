import '../shared_preferences_service.dart';
import 'plant_request_service.dart';
import 'room_request_service.dart';
import 'water_tank_request_service.dart';

Future<Map> getAllRoomsAndRoles() async {
  Map data = {};

  data['roles'] = await getRoles();
  data['rooms'] = await getAllRooms();

  return data;
}

Future<Map> getRoomContent({
  required int roomId,
}) async {
  Map data = {};

  data['roles'] = await getRoles();
  data['plants'] = await getAllPlantsInRoom(roomId: roomId);
  data['waterTank'] = await getWaterTank(roomId: roomId);

  return data;
}

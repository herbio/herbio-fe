import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../constants/api_app_paths.dart';
import '../../../modules/models/entities/water_tank.dart';
import '../shared_preferences_service.dart';
import 'measurement_request_service.dart';

Future<WaterTank?> getWaterTank({
  required int roomId,
}) async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  // ignore: avoid_init_to_null
  WaterTank? waterTank = null;

  Map<String, double> waterTankMeasurements = await getWaterTankMeasurements();

  try {
    var response = await client.get(
      Uri.parse('$ROOMS/$roomId/waterTanks'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${_jwtToken!}'
      },
    );

    if (response.statusCode == 200) {
      List<Map> responseWaterTanks = List<Map>.from(
        jsonDecode(response.body) as List,
      );

      waterTank = WaterTank(
        id: responseWaterTanks[0]['id'],
        capacity: responseWaterTanks[0]['capacity'],
        lastTimeFilled: DateTime.parse(responseWaterTanks[0]['updated']),
        lastCycle: 0.0,
        longestCycle: 0.0,
        shortestCycle: 0.0,
        waterLevel: 0.1, //waterTankMeasurements[waterT['id'].toString()]!,
      );
    } else {
      print(
        'GetAllWaterTanks - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }

  return waterTank;
}

Future<void> createWaterTank({
  required int roomId,
  required double capacity,
}) async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  try {
    var response = await client.post(
      Uri.parse('$ROOMS/$roomId/waterTanks'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${_jwtToken!}'
      },
      body: jsonEncode(
        <String, double>{
          'capacity': capacity,
        },
      ),
    );

    if (response.statusCode >= 300) {
      print(
        'AddWaterTank - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }
}

Future<void> updateWaterTank({
  required int roomId,
  required int waterTankId,
  required double newCapacity,
}) async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  try {
    var response = await client.put(
      Uri.parse('$ROOMS/$roomId/waterTanks/$waterTankId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${_jwtToken!}'
      },
      body: jsonEncode(
        <String, double>{
          'capacity': newCapacity,
        },
      ),
    );

    if (response.statusCode >= 300) {
      print(
        'EditWaterTank - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }
}

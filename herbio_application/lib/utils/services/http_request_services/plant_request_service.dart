import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../../constants/api_app_paths.dart';
import '../../../modules/models/entities/plant.dart';
import '../../../modules/models/types/watering_mode.dart';
import '../../placeholder_data.dart';
import '../shared_preferences_service.dart';
import 'measurement_request_service.dart';
import 'watering_mode_request_service.dart';

Future<List<Plant>> getAllPlantsInRoom({
  required int roomId,
}) async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  List<Plant> plants = [];

  Map<String, Map> plantMeasurements = await getPlantMeasurements();

  try {
    var response = await client.get(
      Uri.parse('$ROOMS/$roomId/plants'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${_jwtToken!}'
      },
    );

    if (response.statusCode == 200) {
      List<Map> responsePlants = List<Map>.from(
        jsonDecode(response.body) as List,
      );

      responsePlants.forEach(
        (plant) {
          plants.add(
            Plant(
              id: plant['id'],
              plantName: plant['name'],
              humidity: 0.0,
              wateringRequirement: plant['waterRequirement'],
              lightingRequirement:
                  lightingMapping[plant['lightingRequirement']]!['value'],
              soilTemperature: 0.0,
              wateringAmount: plant['wateringProfile']['wateringAmount'],
              wateringMode: plant['wateringProfile']['wateringMode'][0] == 'M'
                  ? WateringMode.MANUAL
                  : WateringMode.AUTO,
              lowerWaterThreshold: plant['wateringProfile']
                  ['humidityThresholdDown'],
              upperWaterThreshold: plant['wateringProfile']
                  ['humidityThresholdUp'],
            ),
          );
        },
      );
    } else {
      print(
        'GetAllPlants - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }

  return plants;
}

void createPlant({
  required int roomId,
  required String plantName,
  required String lightingRequirement,
  required int waterRequirement,
  required String wateringMode,
  required int wateringAmount,
  required double lowerWaterThreshold,
  required double upperWaterThreshold,
}) async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  try {
    var response = await client.post(
      Uri.parse('$ROOMS/$roomId/plants'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${_jwtToken!}'
      },
      body: jsonEncode(
        <String, dynamic>{
          'description': '$plantName description',
          'lightingRequirement': lightingRequirement,
          'name': plantName,
          'waterRequirement': waterRequirement,
        },
      ),
    );

    if (response.statusCode == 200) {
      await createWateringMode(
        plantId: jsonDecode(response.body)['id'],
        wateringMode: wateringMode,
        wateringAmount: wateringAmount,
        lowerWaterThreshold: lowerWaterThreshold,
        upperWaterThreshold: upperWaterThreshold,
      );
    } else {
      print(
        'CreatePlant - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }
}

void updatePlant({
  required int roomId,
  required int plantId,
  required String plantName,
  required String lightingRequirement,
  required int waterRequirement,
  required String wateringMode,
  required int wateringAmount,
  required double lowerWaterThreshold,
  required double upperWaterThreshold,
}) async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  try {
    var response = await client.put(
      Uri.parse('$ROOMS/$roomId/plants/$plantId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${_jwtToken!}'
      },
      body: jsonEncode(
        <String, dynamic>{
          'description': '$plantName description',
          'lightingRequirement': lightingRequirement,
          'name': plantName,
          'waterRequirement': waterRequirement,
        },
      ),
    );

    if (response.statusCode == 200) {
      Map<String, dynamic>? oldWateringMode =
          await getWateringMode(plantId: plantId);

      if (oldWateringMode!['humidityThresholdDown'] != lowerWaterThreshold ||
          oldWateringMode['humidityThresholdUp'] != upperWaterThreshold ||
          oldWateringMode['wateringAmount'] != wateringAmount ||
          oldWateringMode['wateringMode'] != wateringMode) {
        updateWateringMode(
          wateringModeId: oldWateringMode['id'],
          wateringMode: wateringMode,
          wateringAmount: wateringAmount,
          lowerWaterThreshold: lowerWaterThreshold,
          upperWaterThreshold: upperWaterThreshold,
        );
      }
    } else {
      print(
        'UpdatePlant - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }
}

void deletePlant({
  required int roomId,
  required int plantId,
}) async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  try {
    var response = await client.delete(
      Uri.parse('$ROOMS/$roomId/plants/$plantId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${_jwtToken!}'
      },
    );

    if (response.statusCode >= 300) {
      print(
        'RemovePlant - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }
}

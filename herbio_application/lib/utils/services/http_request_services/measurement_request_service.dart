import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../placeholder_data.dart';
import '../shared_preferences_service.dart';

Future<Map<String, Map>> getPlantMeasurements() async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  Map<String, Map> plantMeasurements = {};

  try {
    // var response = await client.get(
    //   Uri.parse(WATERTANK_MESUREMENT),
    //   headers: <String, String>{
    //     'Content-Type': 'application/json; charset=UTF-8',
    //     'Authorization': 'Bearer ${_jwtToken!}'
    //   },
    // );

    // if (response.statusCode == 200) {
    //   print(response.body);
    //   List<Map> responseWaterTankMeasurements =
    //       List<Map>.from(jsonDecode(response.body) as List);

    //   responseWaterTankMeasurements.forEach(
    //     (element) {
    //       waterLevels[element['id'].toString()] = element['waterLevel'];
    //     },
    //   );
    // } else {
    //   print('Something went wrong, server returned ${response.statusCode}');
    // }

    List<Map> responseLevels = List<Map>.from(
      jsonDecode(plantMeasurementsResponsebody) as List,
    );

    responseLevels.forEach(
      (measurement) {
        plantMeasurements[measurement['id'].toString()] = measurement;
      },
    );
  } finally {
    client.close();
  }

  return plantMeasurements;
}

Future<Map<String, Map>> getRoomMeasurements() async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  Map<String, Map> roomMeasurements = {};

  try {
    // var response = await client.get(
    //   Uri.parse(ROOM_MESUREMENT),
    //   headers: <String, String>{
    //     'Content-Type': 'application/json; charset=UTF-8',
    //     'Authorization': 'Bearer ${_jwtToken!}'
    //   },
    // );

    // if (response.statusCode == 200) {
    //   List<Map> responseRoomMeasurements =
    //       List<Map>.from(jsonDecode(response.body) as List);

    //   responseRoomMeasurements.forEach(
    //     (measurement) {
    //       roomMeasurements[measurement['id'].toString()] = measurement;
    //     },
    //   );
    // } else {
    //   print('Something went wrong! Server returned ${response.statusCode}!');
    // }

    List<Map> responseRoomMeasurements = List<Map>.from(
      jsonDecode(roomMeasurementsResponsebody) as List,
    );

    responseRoomMeasurements.forEach(
      (measurement) {
        roomMeasurements[measurement['id'].toString()] = measurement;
      },
    );
  } finally {
    client.close();
  }

  return roomMeasurements;
}

Future<Map<String, double>> getWaterTankMeasurements() async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  Map<String, double> waterTankMeasurements = {};

  try {
    // var response = await client.get(
    //   Uri.parse(WATERTANK_MEASUREMENT),
    //   headers: <String, String>{
    //     'Content-Type': 'application/json; charset=UTF-8',
    //     'Authorization': 'Bearer ${_jwtToken!}'
    //   },
    // );

    // if (response.statusCode == 200) {
    //   print(response.body);
    //   List<Map> responseWaterTankMeasurements =
    //       List<Map>.from(jsonDecode(response.body) as List);

    //   waterTankMeasurements[responseWaterTankMeasurements[0]['id'].toString()] =
    //       responseWaterTankMeasurements[0]['waterLevel'];

    // responseWaterTankMeasurements.forEach(
    //   (element) {
    //     waterTankMeasurements[element['id'].toString()] =
    //         element['waterLevel'];
    //   },
    // );
    // } else {
    //   print(
    //     'Something went wrong, server returned ${response.statusCode}',
    //   );
    // }

    List<Map> responseLevels = List<Map>.from(
      jsonDecode(waterTankMeasurementsResponsebody) as List,
    );

    responseLevels.forEach(
      (measurement) {
        waterTankMeasurements[measurement['id'].toString()] =
            measurement['waterLevel'];
      },
    );
  } finally {
    client.close();
  }
  return waterTankMeasurements;
}

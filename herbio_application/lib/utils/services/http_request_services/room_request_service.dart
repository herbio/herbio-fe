import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:routemaster/routemaster.dart';

import '../../../constants/api_app_paths.dart';
import '../../../modules/models/entities/room.dart';
import '../../../modules/models/entities/water_tank.dart';
import '../../../modules/models/notifications/herbio_notification.dart';
import '../../../modules/models/types/notification_type.dart';
import '../shared_preferences_service.dart';
import 'measurement_request_service.dart';
import 'water_tank_request_service.dart';

Future<List<Room>> getAllRooms() async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  List<Room> allRooms = [];

  Map<String, double> waterTankMeasurements = await getWaterTankMeasurements();

  Map<String, Map> roomMeasurements = await getRoomMeasurements();

  await Future.delayed(
    const Duration(milliseconds: 500),
    () async {
      try {
        var response = await client.get(
          Uri.parse(ROOMS),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': 'Bearer ${_jwtToken!}'
          },
        );

        if (response.statusCode == 200) {
          List<Map> responseRooms = List<Map>.from(
            jsonDecode(response.body) as List,
          );

          responseRooms.forEach(
            (room) {
              List<HerbioNotification> notifications = [];

              if (waterTankMeasurements[
                      room['waterTanks'][0]['id'].toString()]! <
                  0.15) {
                notifications.add(
                  HerbioNotification(
                    icon: Icon(
                      Icons.water_drop,
                      color: Colors.blue,
                    ),
                    type: NotificationType.EMPTY_TANK,
                    notificationText:
                        'The water level in the tank is below the minimum level ( 15 % ).  Please refill at your earliest convenience!',
                  ),
                );
              }

              allRooms.add(
                Room(
                  id: room['id'],
                  roomName: room['name'],
                  lightingLevel:
                      roomMeasurements[room['id'].toString()]!['lighting']
                          .round(),
                  waterTank: WaterTank(
                    id: room['waterTanks'][0]['id'],
                    capacity: room['waterTanks'][0]['capacity'],
                    waterLevel: waterTankMeasurements[
                        room['waterTanks'][0]['id'].toString()]!,
                    lastTimeFilled:
                        DateTime.parse(room['waterTanks'][0]['updated']),
                    shortestCycle: 0,
                    longestCycle: 0,
                    lastCycle: 0,
                  ),
                  temperature:
                      roomMeasurements[room['id'].toString()]!['temperature'],
                  plants: [],
                  notifications: notifications,
                ),
              );
            },
          );
        } else {
          print(
            'GetAllRooms - Something went wrong, server returned: ${response.statusCode}',
          );
        }
      } finally {
        client.close();
      }
    },
  );

  return allRooms;
}

void createRoom({
  required String roomName,
  required double waterTankCapacity,
}) async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  try {
    var response = await client.post(
      Uri.parse(ROOMS),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${_jwtToken!}'
      },
      body: jsonEncode(
        <String, String>{
          'description': '$roomName description',
          'name': roomName,
        },
      ),
    );

    if (response.statusCode == 200) {
      await createWaterTank(
        roomId: jsonDecode(response.body)['id'],
        capacity: waterTankCapacity,
      );
    } else {
      print(
        'AddRoom - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }
}

void updateRoom({
  required BuildContext context,
  required int roomId,
  required String roomName,
  required double waterTankCapacity,
}) async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  WaterTank? waterTank = await getWaterTank(roomId: roomId);

  try {
    var response = await client.put(
      Uri.parse('$ROOMS/$roomId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${_jwtToken!}'
      },
      body: jsonEncode(
        <String, String>{
          'description': '$roomName description',
          'name': roomName,
        },
      ),
    );

    if (response.statusCode == 200) {
      print(roomName);
      if (waterTank!.capacity != waterTankCapacity) {
        await updateWaterTank(
          roomId: roomId,
          waterTankId: waterTank.id,
          newCapacity: waterTankCapacity,
        );
        Routemaster.of(context).replace(
          Routemaster.of(context).currentRoute.toString(),
        );
      }
    } else {
      print(
        'EditRoom - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }
}

void removeRoom({
  required int roomId,
}) async {
  String? _jwtToken = await getToken();

  var client = http.Client();

  try {
    var response = await client.delete(
      Uri.parse('$ROOMS/$roomId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ${_jwtToken!}'
      },
    );

    if (response.statusCode >= 300) {
      print(
        'RemoveRoom - Something went wrong, server returned: ${response.statusCode}',
      );
    }
  } finally {
    client.close();
  }
}

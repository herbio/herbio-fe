import 'package:flutter/material.dart';

class PasswordVisibilityProvider with ChangeNotifier {
  bool _isPasswordHidden;

  PasswordVisibilityProvider([this._isPasswordHidden = true]);

  bool get isPasswordHidden {
    return _isPasswordHidden;
  }

  set setIsPasswordHidden(bool isHidden) {
    _isPasswordHidden = isHidden;
  }

  void changePasswordVisibility() {
    _isPasswordHidden = !_isPasswordHidden;
    notifyListeners();
  }
}

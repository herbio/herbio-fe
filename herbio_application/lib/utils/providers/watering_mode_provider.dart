import 'package:flutter/material.dart';

import '../../modules/models/types/watering_mode.dart';

class WateringModeProvider with ChangeNotifier {
  WateringMode _wateringMode = WateringMode.MANUAL;

  WateringMode get wateringMode {
    return _wateringMode;
  }

  void setWateringMode(WateringMode newWateringMode) {
    _wateringMode = newWateringMode;
  }
}

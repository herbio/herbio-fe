import 'dart:typed_data';

import 'package:flutter/material.dart';

class ImagePathProvider with ChangeNotifier {
  String _imagePath = '';
  Uint8List _image = Uint8List(10);

  String get imagePath {
    return _imagePath;
  }

  void setImagePath(String newImagePath) {
    _imagePath = newImagePath;
  }

  Uint8List get image {
    return _image;
  }

  void setImage(Uint8List newImage) {
    _image = newImage;
  }
}

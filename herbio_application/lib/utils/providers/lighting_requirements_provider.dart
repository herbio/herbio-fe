import 'package:flutter/material.dart';

class LightingRequirementsProvider with ChangeNotifier {
  int _lightingRequirement = 0;

  int get lightingRequirement {
    return _lightingRequirement;
  }

  void setLightingRequirement(int newLightingRequirement) {
    _lightingRequirement = newLightingRequirement;
  }
}

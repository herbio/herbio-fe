import 'package:flutter/material.dart';

class DateProvider with ChangeNotifier {
  DateTime _startDate = DateTime.now();
  DateTime _endDate = DateTime.now();

  DateTime get startDate {
    return this._startDate;
  }

  DateTime get endDate {
    return this._endDate;
  }

  void setEndDate(endDate) {
    this._endDate = endDate;
  }

  void setStartDate(startDate) {
    this._startDate = startDate;
  }
}

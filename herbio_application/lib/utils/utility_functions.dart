import 'dart:math';

import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:herbio_application/utils/providers/date_provider.dart';

import '../modules/models/chart_data/line_chart_data.dart';
import '../modules/models/entities/room.dart';

void launchURL(String url) async {
  await launchUrl(Uri.parse(url));
}

Room? getRoom(List<Room> rooms, String roomId) {
  // ignore: avoid_init_to_null
  Room? selectedRoom = null;

  rooms.forEach((Room room) {
    if (room.id == int.parse(roomId.split('=')[1])) {
      selectedRoom = room;
    }
  });

  return selectedRoom;
}

double doubleInRange({
  required Random generator,
  required num start,
  required num end,
}) {
  return generator.nextDouble() * (end - start) + start;
}

List<LineChartData> generateChartData({
  required List<Room> rooms,
  required DateProvider dateProvider,
  required int min,
  required int max,
}) {
  Random _randomGenerator = Random();
  List<LineChartData> _data = [];

  if (dateProvider.endDate.difference(dateProvider.startDate).inDays > 0) {
    for (int i = 0;
        i <= dateProvider.endDate.difference(dateProvider.startDate).inDays;
        i++) {
      Map<String, double> _values = {};

      rooms.forEach(
        (room) {
          _values[room.roomName] = doubleInRange(
            generator: _randomGenerator,
            start: min,
            end: max,
          );
        },
      );

      _data.add(
        LineChartData(
          DateFormat('yyyy-MM-dd').format(
            dateProvider.startDate.add(
              Duration(
                days: i,
              ),
            ),
          ),
          _values,
        ),
      );
    }
  } else {
    for (int i = 0;
        i <
            dateProvider.endDate
                .difference(dateProvider.startDate.subtract(
                    Duration(days: 1, minutes: dateProvider.startDate.minute)))
                .inHours;
        i++) {
      Map<String, double> _values = {};

      rooms.forEach(
        (room) {
          _values[room.roomName] = doubleInRange(
            generator: _randomGenerator,
            start: min,
            end: max,
          );
        },
      );

      _data.add(
        LineChartData(
          DateFormat('HH:mm').format(
            dateProvider.startDate.add(
              Duration(
                hours: i,
              ),
            ),
          ),
          _values,
        ),
      );
    }
  }

  return _data;
}

List<LineSeries<LineChartData, String>> getLineSeries({
  required List<Room> rooms,
  required DateProvider dateProvider,
  required int min,
  required int max,
}) {
  List<LineSeries<LineChartData, String>> _lineSeries = [];

  rooms.forEach(
    (room) {
      _lineSeries.add(
        LineSeries<LineChartData, String>(
          animationDuration: 2500,
          dataSource: generateChartData(
            rooms: rooms,
            dateProvider: dateProvider,
            min: min,
            max: max,
          ),
          xValueMapper: (LineChartData data, _) => data.timeIndicator,
          yValueMapper: (LineChartData data, _) => data.values[room.roomName],
          width: 2,
          name: room.roomName,
          markerSettings: const MarkerSettings(
            isVisible: false,
          ),
          opacity: 0.6,
        ),
      );
    },
  );

  return _lineSeries;
}

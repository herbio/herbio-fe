import '../modules/models/entities/plant.dart';
import '../modules/models/entities/room.dart';
import '../modules/models/entities/water_tank.dart';
import '../modules/models/types/watering_mode.dart';

Room newRoom = Room(
  id: -1,
  roomName: '',
  lightingLevel: 0,
  waterTank: WaterTank(
    capacity: -1,
    id: -1,
    lastCycle: 0,
    lastTimeFilled: DateTime.now(),
    longestCycle: 0,
    shortestCycle: 0,
    waterLevel: 0.0,
  ),
  temperature: 0,
  plants: [],
  notifications: [],
);

Plant newPlant = Plant(
  id: 0,
  plantName: '',
  wateringRequirement: -1,
  lightingRequirement: 0,
  humidity: -1.0,
  soilTemperature: -1.0,
  lowerWaterThreshold: -1.0,
  upperWaterThreshold: -1.0,
  wateringAmount: -1,
  wateringMode: WateringMode.MANUAL,
);

Map<String, Map<String, dynamic>> lightingMapping = {
  'IGNORE': {
    'value': 0,
    'range': '',
  },
  'VERY_LOW': {
    'value': 1,
    'range': '<75 Lux',
  },
  'LOW': {
    'value': 2,
    'range': '(100, 150) Lux',
  },
  'MEDIUM': {
    'value': 3,
    'range': '(150, 200) Lux',
  },
  'HIGH': {
    'value': 4,
    'range': '(200, 250) Lux',
  },
  'VERY_HIGH': {
    'value': 5,
    'range': '+250 Lux',
  },
};

Map<int, Map<String, String>> lightingMappingReversed = {
  0: {
    'value': 'IGNORE',
  },
  1: {
    'value': 'VERY_LOW',
  },
  2: {
    'value': 'LOW',
  },
  3: {
    'value': 'MEDIUM',
  },
  4: {
    'value': 'HIGH',
  },
  5: {
    'value': 'VERY_HIGH',
  },
};

String waterTankMeasurementsResponsebody = '''[{"id":1,"waterLevel":0.1}, 
    {"id":2,"waterLevel":0.4}, 
    {"id":3,"waterLevel":0.1}, 
    {"id":4,"waterLevel":0.4}, 
    {"id":5,"waterLevel":0.1}, 
    {"id":6,"waterLevel":0.4}, 
    {"id":7,"waterLevel":0.1}, 
    {"id":8,"waterLevel":0.4}, 
    {"id":9,"waterLevel":0.1}, 
    {"id":10,"waterLevel":0.4}, 
    {"id":11,"waterLevel":0.1}, 
    {"id":12,"waterLevel":0.4}, 
    {"id":13,"waterLevel":0.1}, 
    {"id":14,"waterLevel":0.4}, 
    {"id":15,"waterLevel":0.1},
    {"id":16,"waterLevel":0.4}]''';

String plantMeasurementsResponsebody =
    '''[{"humidity": 11.2, "id": 1, "temperature": 32.1},
        {"humidity": 22.2, "id": 2, "temperature": 16.1}, 
        {"humidity": 11.2, "id": 3, "temperature": 32.1},
        {"humidity": 22.2, "id": 4, "temperature": 16.1}]''';

String roomMeasurementsResponsebody =
    '''[{"humidity": 30.2, "id": 1, "lighting": 3.5, "temperature": 20.5},
        {"humidity": 81.2, "id": 2, "lighting": 4.5, "temperature": 11.5},
        {"humidity": 81.2, "id": 3, "lighting": 4.5, "temperature": 11.5}, 
        {"humidity": 30.2, "id": 4, "lighting": 3.5, "temperature": 20.5},
        {"humidity": 81.2, "id": 6, "lighting": 4.5, "temperature": 11.5},
        {"humidity": 81.2, "id": 6, "lighting": 4.5, "temperature": 11.5}, 
        {"humidity": 30.2, "id": 7, "lighting": 3.5, "temperature": 20.5},
        {"humidity": 81.2, "id": 8, "lighting": 4.5, "temperature": 11.5},
        {"humidity": 81.2, "id": 9, "lighting": 4.5, "temperature": 11.5}, 
        {"humidity": 30.2, "id": 10, "lighting": 3.5, "temperature": 20.5},
        {"humidity": 81.2, "id": 11, "lighting": 4.5, "temperature": 11.5},
        {"humidity": 81.2, "id": 12, "lighting": 4.5, "temperature": 11.5}]''';

/// App Bar
const String GIT_LAB_ICON = 'assets/images/icons/gitlab.svg';
const String HERBIO_ICON = 'assets/images/icons/HerbIO_Logo.svg';

/// Landing Screen / Introduction Screens
const String APPLICATION = 'assets/images/introduction_screens/application.png';
const String IMPROVEMENTS =
    'assets/images/introduction_screens/improvements.jpeg';
const String MOTIVATION = 'assets/images/introduction_screens/motivation.jpeg';
const String OFFER = 'assets/images/introduction_screens/offer.jpg';

/// About Us Screen
const String TOMAS = 'assets/images/avatars/avatar_1.png';
const String ADAM = 'assets/images/avatars/avatar_3.png';
const String ENIKO = 'assets/images/avatars/enci.jpg';
const String LACI = 'assets/images/avatars/laci.png';
const String MAKSIM = 'assets/images/avatars/maksim.jpg';
const String PATRIK = 'assets/images/avatars/pato.png';

/// Plant Screen
const String PLANT = 'assets/images/plant.jpeg';

import 'server_path.dart';

const String ROOMS = '$SERVER/api/rooms';

const String WATERING_PROFILES = '$SERVER/api/wateringProfiles';

const String ROOM_MEASUREMENT = '$SERVER/api/measurements/room';

const String WATERTANK_MEASUREMENT = '$SERVER/api/measurements/watertank';

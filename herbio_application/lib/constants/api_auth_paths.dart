import 'server_path.dart';

const String LOGIN = '$SERVER/auth/login/';

const String LOGOUT = '$SERVER/auth/logout';

const String REFRESH = '$SERVER/auth/refresh';

const String SIGNUP = '$SERVER/auth/signup';

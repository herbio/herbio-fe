import 'package:flutter/material.dart';

import '../../../../widgets/appbar/herbio_mobile_app_bar.dart';

class AndroidAboutUsLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HerbIOMobileAppBar(),
      body: Center(
        child: Container(
          child: Text('Android About Us'),
        ),
      ),
    );
  }
}

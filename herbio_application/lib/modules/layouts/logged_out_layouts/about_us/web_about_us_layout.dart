import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../constants/asset_constants.dart';
import '../../../../widgets/appbar/logged_out_appbar/herbio_app_bar.dart';

class WebAboutUsLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HerbIOAppBar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 35.0,
            horizontal: 200.0,
          ).r,
          child: Center(
            child: Column(
              children: <Widget>[
                Text(
                  'Our Team',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorLight,
                    fontSize: 35.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 45.0.h,
                ),
                Text(
                  'Our team consists of three parts: Frontend, Backend and Hardware. The frontend is handled by Enikő and Ladislav, the backend is handled by Patrik and the hardware is handled by Maksim, Adam and Tomáš. We are a team of students trying to develop an improved automated irrigation system. We will now introduce each team member in more detail.',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorLight,
                    fontSize: 25.sp,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 75.0.h,
                ),
                Text(
                  'Maksim Mištec',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorLight,
                    fontSize: 35.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        right: 45.0,
                      ).r,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(
                          500,
                        ),
                        child: Image.asset(
                          MAKSIM,
                          height: 300.h,
                        ),
                      ),
                    ),
                    Flexible(
                      child: Text(
                        "Since 2018 I am studying at the Faculty of Electrical Engineering and Informatics of the Slovak University of Technology. I am currently a 1st year engineering student, majoring in Applied Informatics - Modelling and Simulation of Event Systems. Part of my bachelor's thesis was to perform UX tests on an existing Android application, then analyze them and at the end implement or fix the things that were badly affecting the user experience. My work experience is in UI and UX design for mobile apps, and I'm currently learning how to code in Kotlin.",
                        style: TextStyle(
                          color: Theme.of(context).primaryColorLight,
                          fontSize: 25.sp,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 50.0.h,
                ),
                Text(
                  'Patrik Gúgh',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorLight,
                    fontSize: 35.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: [
                    Flexible(
                      child: Text(
                        "I am a 1st year engineering student at the Faculty of Electrical Engineering and Informatics of the Slovak University of Technology. I study Applied Informatics - Modelling and Simulation of Event Systems. In my bachelor's thesis I have studied the Petriflow modelling language and implementation of processes in an academic information system. Most of my work experience is with web applications, specifically with Java, Kotlin, Typescript and frameworks like Spring, Ktor, Angular. I am currently employed at Innovatrics Ltd. where I hold the position of software developer. Apart from work, I have recently started to work on mobile applications and DevOps technologies in my spare time. I believe that I will be able to capitalize my experience from work and school assignments in this team project.",
                        style: TextStyle(
                          color: Theme.of(context).primaryColorLight,
                          fontSize: 25.sp,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 45.0,
                      ).r,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(
                          500,
                        ),
                        child: Image.asset(
                          PATRIK,
                          height: 300.h,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 50.0.h,
                ),
                Text(
                  'Ladislav Rajcsányi',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorLight,
                    fontSize: 35.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        right: 45.0,
                      ).r,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(
                          500,
                        ),
                        child: Image.asset(
                          LACI,
                          height: 300.h,
                        ),
                      ),
                    ),
                    Flexible(
                      child: Text(
                        "I am a graduate of the Faculty of Electrical Engineering and Informatics of the Slovak University of Technology in Applied Informatics - Modelling and Simulation of Event Systems. In my thesis I worked on the design and implementation of a user interface for time series search. During the development of my thesis I got familiar with the Flutter framework from Google. This framework will allow developers to create applications for different platforms such as Web, Android, iOS, Windows, macOs and Linux. After completing my bachelor's degree, I started working through an agency as a Data Scientist intern in the Bratislava department of a Swiss company. In this team project, I use my experience from Flutter framework to create a web and mobile ( Android ) version of our app.",
                        style: TextStyle(
                          color: Theme.of(context).primaryColorLight,
                          fontSize: 25.sp,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 50.0.h,
                ),
                Text(
                  'Enikő Villantová',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorLight,
                    fontSize: 35.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: [
                    Flexible(
                      child: Text(
                        "I am a student of the Slovak University of Technology in Bratislava. I am studying in the first year of engineering studies at the Faculty of Electrical Engineering and Informatics, major in Applied Informatics - Modelling and Simulation of Event Systems. While writing my bachelor's thesis I was working on JavaFX, which I used to create a desktop didactic application for the subject Linear Algebra 2. During my bachelor's I worked in a company where I was in charge of creating and maintaining automated tests.",
                        style: TextStyle(
                          color: Theme.of(context).primaryColorLight,
                          fontSize: 25.sp,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 45.0,
                      ).r,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(
                          500,
                        ),
                        child: Image.asset(
                          ENIKO,
                          height: 300.h,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 50.0.h,
                ),
                Text(
                  'Tomáš Kukumberg',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorLight,
                    fontSize: 35.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        right: 45.0,
                      ).r,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(
                          500,
                        ),
                        child: Image.asset(
                          TOMAS,
                          height: 300.h,
                        ),
                      ),
                    ),
                    Flexible(
                      child: Text(
                        "I am a student of the Slovak University of Technology in Bratislava. I am studying in the first year of engineering studies at the Faculty of Electrical Engineering and Informatics, major in Applied Informatics - Modelling and Simulation of Event Systems. In my bachelor's thesis, I dealt with the design and programming of an Android chatbot application using NOSQL database. During my engineering studies I started working as a junior web developer, where I have so far encountered the Typo3 CMS system on the backend and a custom CSS bootstrap-like library on the frontend. Alongside school and my part-time job, I'm also learning React + Flask and as a hobby I like to learn new things about the Linux Operating System.",
                        style: TextStyle(
                          color: Theme.of(context).primaryColorLight,
                          fontSize: 25.sp,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 50.0.h,
                ),
                Text(
                  'Adam Samko',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorLight,
                    fontSize: 35.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: [
                    Flexible(
                      child: Text(
                        "I study Applied Informatics at the Faculty of Electrical Engineering and Informatics of the Slovak University of Technology. In my bachelor's thesis, I worked on composing a robot controlled by Raspberry Pi, which was able to communicate with the user using a microphone and a speaker. While working on my bachelor thesis, I gained experience in plugging modules into the Raspberry Pi and preparing the user environment. I currently work as a frontend developer in the Angular framework and am interested in Web3 technologies, reactive Frontend frameworks and Cloud solutions.",
                        style: TextStyle(
                          color: Theme.of(context).primaryColorLight,
                          fontSize: 25.sp,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 45.0,
                      ).r,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(
                          500,
                        ),
                        child: Image.asset(
                          ADAM,
                          height: 300.h,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

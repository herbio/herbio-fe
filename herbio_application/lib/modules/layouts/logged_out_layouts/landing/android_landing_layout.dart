// SOURCE : https://www.youtube.com/watch?v=CQlA2p--oEg

import 'package:flutter/material.dart';

import 'package:introduction_screen/introduction_screen.dart';
import 'package:routemaster/routemaster.dart';

import '../../../../constants/asset_constants.dart';
import '../../../../widgets/appbar/herbio_mobile_app_bar.dart';

class AndroidLandingLayout extends StatelessWidget {
  Widget _buildImage(String path) => Center(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(
              15,
            ),
            child: Image.asset(
              path,
              height: 300,
              fit: BoxFit.cover,
            ),
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    PageDecoration getPageDecoration() => PageDecoration(
          titleTextStyle: TextStyle(
            fontSize: 28,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).colorScheme.secondary,
          ),
          bodyTextStyle: TextStyle(
            color: Theme.of(context).primaryColorLight,
          ),
        );

    return Scaffold(
      appBar: HerbIOMobileAppBar(),
      body: IntroductionScreen(
        pages: [
          PageViewModel(
            image: _buildImage(MOTIVATION),
            title: 'Our Motivation',
            body:
                'Our main motivation for the design and implementation of this project is to create an automated system that will ensure watering of plants without human presence during and after the COVID-19 pandemic. The main reason for this is so that FEI STU staff can attend to more serious tasks and not waste time watering plants. We will design the given system in such a way that it will be scalable in the future.',
            decoration: getPageDecoration(),
          ),
          PageViewModel(
            image: _buildImage(OFFER),
            title: 'What we offer?',
            body:
                "We can provide an extension and especially an improvement of the already proposed solution of last year's Plantio team project, which aimed at the design and implementation of an automated irrigation system with minimal operation. This system can be controlled by a mobile app in a limited mode and can also monitor the status of the pump and the plants using various sensors that are integrated using an Arduino microprocessor.",
            decoration: getPageDecoration(),
          ),
          PageViewModel(
            image: _buildImage(IMPROVEMENTS),
            title: 'Our improvements',
            body:
                "The main improvement will be to design a hardware solution for an automated irrigation system that can operate multiple plant sections using multiple valves connected to the pump. These sections will be individually controllable and each will be able to have its own specific settings. Another innovation we want to bring is the creation of irrigation profiles for different types of plants, which the user will be able to select for a given section. We also want to allow the user to monitor the water level in the tank with the help of an ultrasonic sensor, for better timing of water refilling in the tank.",
            decoration: getPageDecoration(),
          ),
          PageViewModel(
            image: _buildImage(APPLICATION),
            title: 'Our applications',
            body:
                "This will also include the design and development of a web interface and mobile application that can operate such a system. We have members within the team who have experience in creating mobile and web applications as well as designing and wiring microprocessors and their components.",
            decoration: getPageDecoration(),
          ),
        ],
        showNextButton: false,
        done: Text(
          'Register',
          style: TextStyle(
            color: Theme.of(context).colorScheme.secondary,
          ),
        ),
        onDone: () {
          Routemaster.of(context).replace('/register');
        },
      ),
    );
  }
}

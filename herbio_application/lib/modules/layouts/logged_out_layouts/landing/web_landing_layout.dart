import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../constants/asset_constants.dart';
import '../../../../widgets/appbar/logged_out_appbar/herbio_app_bar.dart';

class WebLandingLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HerbIOAppBar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 35.0,
            horizontal: 200.0,
          ).r,
          child: Center(
            child: Column(
              children: <Widget>[
                Text(
                  'Our Motivation',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorLight,
                    fontSize: 35.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        right: 45.0,
                      ).r,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(
                          15,
                        ),
                        child: Image.asset(
                          MOTIVATION,
                          height: 300.h,
                        ),
                      ),
                    ),
                    Flexible(
                      child: Text(
                        'Our main motivation for the design and implementation of this project is to create an automated system that will ensure watering of plants without human presence during and after the COVID-19 pandemic. The main reason for this is so that FEI STU staff can attend to more serious tasks and not waste time watering plants. We will design the given system in such a way that it will be scalable in the future.',
                        style: TextStyle(
                          color: Theme.of(context).primaryColorLight,
                          fontSize: 25.sp,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 50.0.h,
                ),
                Text(
                  'What we offer?',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorLight,
                    fontSize: 35.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: [
                    Flexible(
                      child: Text(
                        "We can provide an extension and especially an improvement of the already proposed solution of last year's Plantio team project, which aimed at the design and implementation of an automated irrigation system with minimal operation. This system can be controlled by a mobile app in a limited mode and can also monitor the status of the pump and the plants using various sensors that are integrated using an Arduino microprocessor.",
                        style: TextStyle(
                          color: Theme.of(context).primaryColorLight,
                          fontSize: 25.sp,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 45.0,
                      ).r,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(
                          15,
                        ),
                        child: Image.asset(
                          OFFER,
                          height: 300.h,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 50.0.h,
                ),
                Text(
                  'Our improvements',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorLight,
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        right: 45.0,
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(
                          15,
                        ),
                        child: Image.asset(
                          IMPROVEMENTS,
                          height: 300.h,
                        ),
                      ),
                    ),
                    Flexible(
                      child: Text(
                        'The main improvement will be to design a hardware solution for an automated irrigation system that can operate multiple plant sections using multiple valves connected to the pump. These sections will be individually controllable and each will be able to have its own specific settings. Another innovation we want to bring is the creation of irrigation profiles for different types of plants, which the user will be able to select for a given section. We also want to allow the user to monitor the water level in the tank with the help of an ultrasonic sensor, for better timing of water refilling in the tank.',
                        style: TextStyle(
                          color: Theme.of(context).primaryColorLight,
                          fontSize: 25.sp,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 50.0.h,
                ),
                Text(
                  'Our applications',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorLight,
                    fontSize: 35.sp,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Row(
                  children: [
                    Flexible(
                      child: Text(
                        'This will also include the design and development of a web interface and mobile application that can operate such a system. We have members within the team who have experience in creating mobile and web applications as well as designing and wiring microprocessors and their components.',
                        style: TextStyle(
                          color: Theme.of(context).primaryColorLight,
                          fontSize: 25.sp,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 45.0,
                      ).r,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(
                          15,
                        ),
                        child: Image.asset(
                          APPLICATION,
                          height: 300.h,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

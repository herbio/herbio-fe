import 'package:flutter/material.dart';
import 'package:routemaster/routemaster.dart';

import '../../../../widgets/appbar/herbio_mobile_app_bar.dart';
import '../../../../widgets/forms/login_form.dart';

class AndroidSignInLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HerbIOMobileAppBar(),
      body: Center(
        child: Center(
          child: Stack(
            alignment: Alignment.center,
            clipBehavior: Clip.none,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 200,
                    width: 175,
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColorLight,
                      borderRadius: BorderRadius.circular(
                        30.0,
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            'Sign in',
                          ),
                        ),
                      ],
                    ),
                    transform: Matrix4.translationValues(0, -90.0, 0.0),
                  ),
                  GestureDetector(
                    child: Padding(
                      padding: const EdgeInsets.only(
                        bottom: 180.0,
                      ),
                      child: Container(
                        height: 200,
                        width: 175,
                        decoration: BoxDecoration(
                          color: Theme.of(context).colorScheme.secondary,
                          borderRadius: BorderRadius.circular(
                            30.0,
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                'Register',
                                style: TextStyle(
                                  color: Theme.of(context).primaryColorLight,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    onTap: () {
                      Routemaster.of(context).replace('/register');
                    },
                  )
                ],
              ),
              Container(
                width: 350,
                height: 300,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColorLight,
                  borderRadius: BorderRadius.circular(
                    30.0,
                  ),
                ),
                child: Column(
                  children: [
                    SizedBox(
                      height: 50,
                    ),
                    LoginForm(
                      redirectPath: '/',
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:herbio_application/widgets/appbar/logged_in_appbar/herbio_logged_in_app_bar.dart';

import '../../../../utils/utility_functions.dart';
import '../../../../utils/providers/date_provider.dart';
import '../../../../widgets/buttons/rounded_button.dart';
import '../../../models/entities/room.dart';

class WebStatisticsLayout extends StatefulWidget {
  final List<Room> rooms;

  WebStatisticsLayout({
    required this.rooms,
  });

  @override
  State<WebStatisticsLayout> createState() => _WebStatisticsLayoutState();
}

class _WebStatisticsLayoutState extends State<WebStatisticsLayout> {
  @override
  Widget build(BuildContext context) {
    const Color secondaryColor = const Color(0xFF16A085);

    DateProvider _dateProvider = Provider.of<DateProvider>(context);

    void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
      if (args.value is PickerDateRange) {
        _dateProvider.setStartDate(args.value.startDate);
        _dateProvider.setEndDate(args.value.endDate ?? args.value.startDate);
        _dateProvider.notifyListeners();
      }
    }

    Widget getDateRangePicker() {
      return Container(
        height: 500,
        width: 500,
        child: SfDateRangePicker(
          todayHighlightColor: secondaryColor,
          selectionColor: secondaryColor,
          startRangeSelectionColor: secondaryColor,
          rangeSelectionColor: secondaryColor.withOpacity(0.2),
          endRangeSelectionColor: secondaryColor,
          view: DateRangePickerView.month,
          selectionMode: DateRangePickerSelectionMode.range,
          onSelectionChanged: _onSelectionChanged,
          minDate: DateTime(2022, 5, 9, 0, 0, 0),
          maxDate: DateTime.now(),
        ),
      );
    }

    return Scaffold(
      appBar: HerbIOLoggedInAppBar(),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 25.0,
            horizontal: 60.0,
          ),
          child: Center(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RoundedButton(
                      Center(
                        child: Row(
                          children: [
                            Icon(
                              Icons.event,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 10.0,
                              ),
                              child: Text(
                                'Select Date',
                                style: TextStyle(
                                  fontSize: 15,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(
                                    25.0,
                                  ),
                                ),
                              ),
                              title: Center(child: const Text('Select Date')),
                              content: Container(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    getDateRangePicker(),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Text(
                                        'Confirm',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .secondary,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      },
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 20,
                  ),
                  child: Container(
                    height: 450,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(
                          25,
                        ),
                      ),
                      color: Theme.of(context).primaryColorLight,
                    ),
                    child: SfCartesianChart(
                      plotAreaBorderWidth: 0,
                      title: ChartTitle(text: 'Water Levels'),
                      legend: Legend(
                        isVisible: true,
                        overflowMode: LegendItemOverflowMode.wrap,
                        position: LegendPosition.bottom,
                      ),
                      primaryXAxis: CategoryAxis(
                          edgeLabelPlacement: EdgeLabelPlacement.shift,
                          majorGridLines: const MajorGridLines(width: 0)),
                      primaryYAxis: NumericAxis(
                        labelFormat: '{value} %',
                        axisLine: const AxisLine(width: 0),
                        majorTickLines: const MajorTickLines(
                          color: Colors.transparent,
                        ),
                      ),
                      series: getLineSeries(
                        rooms: widget.rooms,
                        dateProvider: _dateProvider,
                        min: 0,
                        max: 100,
                      ),
                      trackballBehavior: TrackballBehavior(
                        enable: true,
                        activationMode: ActivationMode.singleTap,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 20,
                  ),
                  child: Container(
                    height: 450,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(
                          25,
                        ),
                      ),
                      color: Theme.of(context).primaryColorLight,
                    ),
                    child: SfCartesianChart(
                      plotAreaBorderWidth: 0,
                      title: ChartTitle(
                        text: 'Temperature',
                      ),
                      legend: Legend(
                        isVisible: true,
                        overflowMode: LegendItemOverflowMode.wrap,
                        position: LegendPosition.bottom,
                      ),
                      primaryXAxis: CategoryAxis(
                          edgeLabelPlacement: EdgeLabelPlacement.shift,
                          majorGridLines: const MajorGridLines(width: 0)),
                      primaryYAxis: NumericAxis(
                        labelFormat: '{value} °C',
                        axisLine: const AxisLine(width: 0),
                        majorTickLines: const MajorTickLines(
                          color: Colors.transparent,
                        ),
                      ),
                      series: getLineSeries(
                        rooms: widget.rooms,
                        dateProvider: _dateProvider,
                        min: 25,
                        max: 32,
                      ),
                      trackballBehavior: TrackballBehavior(
                        enable: true,
                        activationMode: ActivationMode.singleTap,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 20,
                    horizontal: 20,
                  ),
                  child: Container(
                    height: 450,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(
                          25,
                        ),
                      ),
                      color: Theme.of(context).primaryColorLight,
                    ),
                    child: SfCartesianChart(
                      plotAreaBorderWidth: 0,
                      title: ChartTitle(text: 'Humidity'),
                      legend: Legend(
                        isVisible: true,
                        overflowMode: LegendItemOverflowMode.wrap,
                        position: LegendPosition.bottom,
                      ),
                      primaryXAxis: CategoryAxis(
                          edgeLabelPlacement: EdgeLabelPlacement.shift,
                          majorGridLines: const MajorGridLines(width: 0)),
                      primaryYAxis: NumericAxis(
                        labelFormat: '{value} %',
                        axisLine: const AxisLine(width: 0),
                        majorTickLines: const MajorTickLines(
                          color: Colors.transparent,
                        ),
                      ),
                      series: getLineSeries(
                        rooms: widget.rooms,
                        dateProvider: _dateProvider,
                        min: 55,
                        max: 60,
                      ),
                      trackballBehavior: TrackballBehavior(
                        enable: true,
                        activationMode: ActivationMode.singleTap,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

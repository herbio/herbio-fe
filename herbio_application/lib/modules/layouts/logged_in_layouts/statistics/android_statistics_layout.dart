import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../../../utils/utility_functions.dart';
import '../../../../utils/providers/date_provider.dart';
import '../../../models/entities/room.dart';

class AndroidStatisticsLayout extends StatefulWidget {
  final List<Room> rooms;

  AndroidStatisticsLayout({
    required this.rooms,
  });

  @override
  State<AndroidStatisticsLayout> createState() =>
      _AndroidStatisticsLayoutState();
}

class _AndroidStatisticsLayoutState extends State<AndroidStatisticsLayout> {
  @override
  Widget build(BuildContext context) {
    DateProvider _dateProvider = Provider.of<DateProvider>(context);

    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 20,
              horizontal: 20,
            ),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(
                    25,
                  ),
                ),
                color: Theme.of(context).primaryColorLight,
              ),
              child: SfCartesianChart(
                plotAreaBorderWidth: 0,
                title: ChartTitle(text: 'Water Levels'),
                legend: Legend(
                  isVisible: true,
                  overflowMode: LegendItemOverflowMode.wrap,
                  position: LegendPosition.bottom,
                ),
                primaryXAxis: CategoryAxis(
                    edgeLabelPlacement: EdgeLabelPlacement.shift,
                    majorGridLines: const MajorGridLines(width: 0)),
                primaryYAxis: NumericAxis(
                  labelFormat: '{value} %',
                  axisLine: const AxisLine(width: 0),
                  majorTickLines: const MajorTickLines(
                    color: Colors.transparent,
                  ),
                ),
                series: getLineSeries(
                  rooms: widget.rooms,
                  dateProvider: _dateProvider,
                  min: 0,
                  max: 100,
                ),
                trackballBehavior: TrackballBehavior(
                  enable: true,
                  activationMode: ActivationMode.singleTap,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 20,
              horizontal: 20,
            ),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(
                    25,
                  ),
                ),
                color: Theme.of(context).primaryColorLight,
              ),
              child: SfCartesianChart(
                plotAreaBorderWidth: 0,
                title: ChartTitle(
                  text: 'Temperature',
                ),
                legend: Legend(
                  isVisible: true,
                  overflowMode: LegendItemOverflowMode.wrap,
                  position: LegendPosition.bottom,
                ),
                primaryXAxis: CategoryAxis(
                    edgeLabelPlacement: EdgeLabelPlacement.shift,
                    majorGridLines: const MajorGridLines(width: 0)),
                primaryYAxis: NumericAxis(
                  labelFormat: '{value} °C',
                  axisLine: const AxisLine(width: 0),
                  majorTickLines: const MajorTickLines(
                    color: Colors.transparent,
                  ),
                ),
                series: getLineSeries(
                  rooms: widget.rooms,
                  dateProvider: _dateProvider,
                  min: 25,
                  max: 32,
                ),
                trackballBehavior: TrackballBehavior(
                  enable: true,
                  activationMode: ActivationMode.singleTap,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 20,
              horizontal: 20,
            ),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(
                    25,
                  ),
                ),
                color: Theme.of(context).primaryColorLight,
              ),
              child: SfCartesianChart(
                plotAreaBorderWidth: 0,
                title: ChartTitle(text: 'Humidity'),
                legend: Legend(
                  isVisible: true,
                  overflowMode: LegendItemOverflowMode.wrap,
                  position: LegendPosition.bottom,
                ),
                primaryXAxis: CategoryAxis(
                    edgeLabelPlacement: EdgeLabelPlacement.shift,
                    majorGridLines: const MajorGridLines(width: 0)),
                primaryYAxis: NumericAxis(
                  labelFormat: '{value} %',
                  axisLine: const AxisLine(width: 0),
                  majorTickLines: const MajorTickLines(
                    color: Colors.transparent,
                  ),
                ),
                series: getLineSeries(
                  rooms: widget.rooms,
                  dateProvider: _dateProvider,
                  min: 55,
                  max: 60,
                ),
                trackballBehavior: TrackballBehavior(
                  enable: true,
                  activationMode: ActivationMode.singleTap,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 70,
          )
        ],
      ),
    );
  }
}

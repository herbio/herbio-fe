import 'package:flutter/material.dart';

import 'package:context_menus/context_menus.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:routemaster/routemaster.dart';

import '../../../../utils/placeholder_data.dart';
import '../../../../utils/providers/lighting_requirements_provider.dart';
import '../../../../utils/providers/watering_mode_provider.dart';
import '../../../../utils/services/action_service.dart';
import '../../../../utils/services/dialog_service.dart';
import '../../../../utils/services/http_request_services/plant_request_service.dart';
import '../../../../widgets/appbar/logged_in_appbar/herbio_logged_in_app_bar.dart';
import '../../../../widgets/buttons/rounded_button.dart';
import '../../../../widgets/cards/plant_card/plant_card.dart';
import '../../../../widgets/cards/water_tank_card/water_tank_card.dart';
import '../../../models/entities/plant.dart';
import '../../../models/entities/water_tank.dart';
import '../../../models/types/action_type.dart';

class WebPlantsLayout extends StatefulWidget {
  final List<String> roles;
  final List<Plant> plants;
  final WaterTank waterTank;
  final int roomId;

  WebPlantsLayout({
    required this.roles,
    required this.plants,
    required this.waterTank,
    required this.roomId,
  });

  @override
  State<WebPlantsLayout> createState() => _WebPlantsLayoutState();
}

class _WebPlantsLayoutState extends State<WebPlantsLayout> {
  @override
  Widget build(BuildContext context) {
    LightingRequirementsProvider lightingRequirementsProvider =
        Provider.of<LightingRequirementsProvider>(context);

    WateringModeProvider wateringModeProvider =
        Provider.of<WateringModeProvider>(context);

    return ContextMenuOverlay(
      cardBuilder: (_, children) => Container(
        padding: EdgeInsets.zero,
        color: Colors.transparent,
        child: Card(
          color: Theme.of(context).primaryColorLight,
          semanticContainer: true,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              15.0,
            ),
          ),
          elevation: 5,
          child: Column(
            children: children,
          ),
        ),
      ),
      buttonStyle: ContextMenuButtonStyle(
        fgColor: Theme.of(context).primaryColor,
        bgColor: Colors.transparent,
        hoverBgColor: Theme.of(context).colorScheme.secondary.withOpacity(
              0.3,
            ),
      ),
      child: Scaffold(
        appBar: HerbIOLoggedInAppBar(),
        body: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 10,
          ).r,
          child: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: ContextMenuRegion(
                  contextMenu: widget.roles.contains('ROLE_ADMIN')
                      ? GenericContextMenu(
                          buttonConfigs: [
                            ContextMenuButtonConfig(
                              'Add new Plant',
                              onPressed: () => executePlantAction(
                                context: context,
                                title: 'Add new Plant',
                                mode: ActionType.ADD,
                                plant: newPlant,
                                roomId: widget.roomId,
                                lightingRequirementsProvider:
                                    lightingRequirementsProvider,
                                wateringModeProvider: wateringModeProvider,
                              ),
                            ),
                          ],
                        )
                      : Container(),
                  child: Column(
                    children: [
                      Center(
                        child: Container(
                          child: WaterTankCard(
                            waterTank: widget.waterTank,
                          ),
                        ),
                      ),
                      widget.plants.isEmpty
                          ? widget.roles.contains('ROLE_ADMIN')
                              ? Padding(
                                  padding: const EdgeInsets.only(
                                    top: 150.0,
                                  ).r,
                                  child: Center(
                                    child: RoundedButton(
                                      Text('Add new Plant'),
                                      () {
                                        executePlantAction(
                                          context: context,
                                          title: 'Add new Plant',
                                          mode: ActionType.ADD,
                                          plant: newPlant,
                                          roomId: widget.roomId,
                                          lightingRequirementsProvider:
                                              lightingRequirementsProvider,
                                          wateringModeProvider:
                                              wateringModeProvider,
                                        );
                                      },
                                    ),
                                  ),
                                )
                              : Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      top: 150.0,
                                    ).r,
                                    child: Text(
                                      'No Plants found in this Room.',
                                      style: TextStyle(
                                        color:
                                            Theme.of(context).primaryColorLight,
                                      ),
                                    ),
                                  ),
                                )
                          : Container(),
                    ],
                  ),
                ),
              ),
              SliverPadding(
                padding: const EdgeInsets.only(
                  bottom: 30,
                ),
              ),
              widget.plants.isNotEmpty
                  ? SliverGrid(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 4,
                        childAspectRatio: (1 / 1.25).h,
                        mainAxisSpacing: 15.0,
                      ),
                      delegate: SliverChildBuilderDelegate(
                        (
                          context,
                          index,
                        ) {
                          return ContextMenuRegion(
                            contextMenu: widget.roles.contains('ROLE_ADMIN')
                                ? GenericContextMenu(
                                    buttonConfigs: [
                                      ContextMenuButtonConfig(
                                        'Edit',
                                        onPressed: () => executePlantAction(
                                          context: context,
                                          title: 'Edit Plant',
                                          mode: ActionType.EDIT,
                                          plant: widget.plants[index],
                                          roomId: widget.roomId,
                                          lightingRequirementsProvider:
                                              lightingRequirementsProvider,
                                          wateringModeProvider:
                                              wateringModeProvider,
                                        ),
                                      ),
                                      ContextMenuButtonConfig(
                                        'Remove',
                                        onPressed: () {
                                          removeDialog(
                                            context: context,
                                            title: 'Remove Plant',
                                            target:
                                                widget.plants[index].plantName,
                                            onPressedFunction: () {
                                              setState(
                                                () {
                                                  Navigator.of(context).pop();

                                                  deletePlant(
                                                    roomId: widget.roomId,
                                                    plantId:
                                                        widget.plants[index].id,
                                                  );

                                                  Routemaster.of(context)
                                                      .replace('/rooms');
                                                },
                                              );
                                            },
                                          );
                                        },
                                      ),
                                    ],
                                  )
                                : Container(),
                            child: PlantCard(
                              plant: widget.plants[index],
                            ),
                          );
                        },
                        childCount: widget.plants.length,
                      ),
                    )
                  : SliverToBoxAdapter(),
              SliverPadding(
                padding: const EdgeInsets.only(
                  bottom: 30,
                ).r,
              )
            ],
          ),
        ),
      ),
    );
  }
}

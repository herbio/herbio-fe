import 'package:flutter/material.dart';

import 'package:focused_menu/focused_menu.dart';
import 'package:focused_menu/modals.dart';
import 'package:provider/provider.dart';
import 'package:routemaster/routemaster.dart';

import '../../../../utils/placeholder_data.dart';
import '../../../../utils/providers/lighting_requirements_provider.dart';
import '../../../../utils/providers/watering_mode_provider.dart';
import '../../../../utils/services/action_service.dart';
import '../../../../utils/services/dialog_service.dart';
import '../../../../utils/services/http_request_services/plant_request_service.dart';
import '../../../../widgets/appbar/herbio_mobile_app_bar.dart';
import '../../../../widgets/cards/plant_card/plant_card.dart';
import '../../../../widgets/cards/water_tank_card/water_tank_card.dart';
import '../../../models/entities/plant.dart';
import '../../../models/entities/water_tank.dart';
import '../../../models/types/action_type.dart';

class AndroidPlantsLayout extends StatefulWidget {
  final List<String> roles;
  final List<Plant> plants;
  final WaterTank waterTank;
  final int roomId;

  AndroidPlantsLayout({
    required this.roles,
    required this.plants,
    required this.waterTank,
    required this.roomId,
  });

  @override
  State<AndroidPlantsLayout> createState() => _AndroidPlantsLayoutState();
}

class _AndroidPlantsLayoutState extends State<AndroidPlantsLayout> {
  @override
  Widget build(BuildContext context) {
    LightingRequirementsProvider lightingRequirementsProvider =
        Provider.of<LightingRequirementsProvider>(context);

    WateringModeProvider wateringModeProvider =
        Provider.of<WateringModeProvider>(context);

    return Scaffold(
      appBar: HerbIOMobileAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 10,
        ),
        child: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Column(
                children: [
                  Center(
                    child: Container(
                      child: WaterTankCard(
                        waterTank: widget.waterTank,
                      ),
                    ),
                  ),
                  widget.plants.isEmpty
                      ? widget.roles.contains('ROLE_ADMIN')
                          ? Padding(
                              padding: const EdgeInsets.only(
                                top: 50,
                              ),
                              child: Center(
                                child: Text(
                                  'Please Add new Plants.',
                                  style: TextStyle(
                                    color: Theme.of(context).primaryColorLight,
                                  ),
                                ),
                              ),
                            )
                          : Padding(
                              padding: const EdgeInsets.only(
                                top: 50,
                              ),
                              child: Center(
                                child: Text(
                                  'No Plants found in this Room.',
                                  style: TextStyle(
                                    color: Theme.of(context).primaryColorLight,
                                  ),
                                ),
                              ),
                            )
                      : Container(),
                ],
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.only(
                bottom: 30,
              ),
            ),
            widget.plants.isNotEmpty
                ? SliverGrid(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 1,
                      childAspectRatio: (1 / 1.5),
                      mainAxisSpacing: 15.0,
                    ),
                    delegate: SliverChildBuilderDelegate(
                      (context, index) {
                        return widget.roles.contains('ROLE_ADMIN')
                            ? FocusedMenuHolder(
                                menuBoxDecoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(
                                    30,
                                  ),
                                ),
                                child: PlantCard(
                                  plant: widget.plants[index],
                                ),
                                onPressed: () {},
                                menuItems: [
                                  FocusedMenuItem(
                                    title: Text('Edit'),
                                    onPressed: () {
                                      executePlantAction(
                                        context: context,
                                        title: 'Edit Plant',
                                        mode: ActionType.EDIT,
                                        plant: widget.plants[index],
                                        roomId: widget.roomId,
                                        lightingRequirementsProvider:
                                            lightingRequirementsProvider,
                                        wateringModeProvider:
                                            wateringModeProvider,
                                      );
                                    },
                                    trailingIcon: Icon(
                                      Icons.edit,
                                    ),
                                  ),
                                  FocusedMenuItem(
                                    title: Text('Remove'),
                                    onPressed: () {
                                      removeDialog(
                                        context: context,
                                        title: 'Remove Plant',
                                        target: widget.plants[index].plantName,
                                        onPressedFunction: () {
                                          setState(
                                            () {
                                              Navigator.of(context).pop();

                                              deletePlant(
                                                roomId: widget.roomId,
                                                plantId:
                                                    widget.plants[index].id,
                                              );

                                              Routemaster.of(context)
                                                  .replace('/');
                                            },
                                          );
                                        },
                                      );
                                    },
                                    backgroundColor: Colors.red,
                                    trailingIcon: Icon(
                                      Icons.delete,
                                    ),
                                  ),
                                ],
                              )
                            : PlantCard(
                                plant: widget.plants[index],
                              );
                      },
                      childCount: widget.plants.length,
                    ),
                  )
                : SliverToBoxAdapter(),
            SliverPadding(
              padding: const EdgeInsets.only(
                bottom: 30,
              ),
            )
          ],
        ),
      ),
      floatingActionButton: widget.roles.contains('ROLE_ADMIN')
          ? FloatingActionButton(
              backgroundColor: Theme.of(context).primaryColor,
              child: Icon(
                Icons.add,
                color: Theme.of(context).colorScheme.secondary,
              ),
              tooltip: 'Add New Plant',
              onPressed: () {
                executePlantAction(
                  context: context,
                  title: 'Add new Plant',
                  mode: ActionType.ADD,
                  plant: newPlant,
                  roomId: widget.roomId,
                  lightingRequirementsProvider: lightingRequirementsProvider,
                  wateringModeProvider: wateringModeProvider,
                );
              },
            )
          : null,
    );
  }
}

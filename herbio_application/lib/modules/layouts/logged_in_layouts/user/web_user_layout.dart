import 'package:flutter/material.dart';

import '../../../../widgets/appbar/logged_in_appbar/herbio_logged_in_app_bar.dart';

class WebUserLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: HerbIOLoggedInAppBar(),
      body: Center(
        child: Container(
          child: Text(
            'Web User Page',
            style: TextStyle(
              color: Theme.of(context).primaryColorLight,
            ),
          ),
        ),
      ),
    );
  }
}

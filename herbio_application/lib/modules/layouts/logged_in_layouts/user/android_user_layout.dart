import 'package:flutter/material.dart';
import 'package:routemaster/routemaster.dart';

import 'package:shared_preferences/shared_preferences.dart';

import '../../../../widgets/buttons/rounded_button.dart';
import '../../../screens/logged_out_screens/sign_in_screen.dart';
import '../../../screens/screen.dart';

class AndroidUserLayout extends StatefulWidget {
  @override
  State<AndroidUserLayout> createState() => _AndroidUserLayoutState();
}

class _AndroidUserLayoutState extends State<AndroidUserLayout> {
  void _logout(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.clear();

    Screen _redirectScreen = SignInScreen();

    Routemaster.of(context).replace(_redirectScreen.getRouteName());
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: RoundedButton(
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(
              Icons.logout,
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              'Logout',
              style: TextStyle(
                color: Theme.of(context).primaryColorLight,
              ),
            ),
          ],
        ),
        () {
          _logout(context);
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';

import 'package:focused_menu/focused_menu.dart';
import 'package:focused_menu/modals.dart';
import 'package:routemaster/routemaster.dart';

import '../../../../utils/services/action_service.dart';
import '../../../../utils/services/dialog_service.dart';
import '../../../../utils/services/http_request_services/room_request_service.dart';
import '../../../../widgets/cards/room_card/room_card.dart';
import '../../../models/entities/room.dart';
import '../../../models/types/action_type.dart';

class AndroidRoomsLayout extends StatefulWidget {
  final List<String> roles;
  final List<Room> rooms;

  AndroidRoomsLayout({
    required this.roles,
    required this.rooms,
  });

  @override
  State<AndroidRoomsLayout> createState() => _AndroidRoomsLayoutState();
}

class _AndroidRoomsLayoutState extends State<AndroidRoomsLayout> {
  @override
  Widget build(BuildContext context) {
    return widget.rooms.isEmpty
        ? widget.roles.contains('ROLE_ADMIN')
            ? Center(
                child: Text(
                  'Please add New Room.',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorLight,
                  ),
                ),
              )
            : Center(
                child: Text(
                  'No Rooms found.',
                  style: TextStyle(
                    color: Theme.of(context).primaryColorLight,
                  ),
                ),
              )
        : Padding(
            padding: EdgeInsets.symmetric(
              vertical: 15,
            ),
            child: Center(
              child: ListView.builder(
                itemCount: widget.rooms.length,
                itemBuilder: (BuildContext context, int _index) {
                  return widget.roles.contains('ROLE_ADMIN')
                      ? FocusedMenuHolder(
                          child: RoomCard(
                            index: _index,
                            rooms: widget.rooms,
                          ),
                          onPressed: () {
                            Routemaster.of(context).replace(
                              '/plants/id=${widget.rooms[_index].id}',
                            );
                          },
                          menuItems: [
                            FocusedMenuItem(
                              title: Text('Open'),
                              onPressed: () {
                                Routemaster.of(context).replace(
                                  '/plants/id=${widget.rooms[_index].id}',
                                );
                              },
                              trailingIcon: Icon(
                                Icons.meeting_room,
                              ),
                            ),
                            FocusedMenuItem(
                              title: Text('Edit'),
                              onPressed: () {
                                executeRoomAction(
                                  context: context,
                                  title: 'Edit Room',
                                  room: widget.rooms[_index],
                                  mode: ActionType.EDIT,
                                );
                                Routemaster.of(context).replace('/');
                              },
                              trailingIcon: Icon(
                                Icons.edit,
                              ),
                            ),
                            FocusedMenuItem(
                              title: Text('Remove'),
                              onPressed: () {
                                removeDialog(
                                  context: context,
                                  title: 'Remove Room',
                                  target: widget.rooms[_index].roomName,
                                  onPressedFunction: () {
                                    setState(
                                      () {
                                        Navigator.of(context).pop();
                                        removeRoom(
                                          roomId: widget.rooms[_index].id,
                                        );
                                        Routemaster.of(context).replace(
                                          '/',
                                        );
                                      },
                                    );
                                  },
                                );
                              },
                              backgroundColor: Colors.red,
                              trailingIcon: Icon(
                                Icons.delete,
                              ),
                            ),
                          ],
                        )
                      : FocusedMenuHolder(
                          child: RoomCard(
                            index: _index,
                            rooms: widget.rooms,
                          ),
                          onPressed: () {
                            Routemaster.of(context).replace(
                              '/plants/id=${widget.rooms[_index].id}',
                            );
                          },
                          menuItems: [
                            FocusedMenuItem(
                              title: Text('Open'),
                              onPressed: () {
                                Routemaster.of(context).replace(
                                  '/plants/id=${widget.rooms[_index].id}',
                                );
                              },
                              trailingIcon: Icon(
                                Icons.meeting_room,
                              ),
                            ),
                          ],
                        );
                },
              ),
            ),
          );
  }
}

import 'package:flutter/material.dart';

import 'package:context_menus/context_menus.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:routemaster/routemaster.dart';

import '../../../../utils/placeholder_data.dart';
import '../../../../utils/services/action_service.dart';
import '../../../../utils/services/dialog_service.dart';
import '../../../../utils/services/http_request_services/room_request_service.dart';
import '../../../../widgets/appbar/logged_in_appbar/herbio_logged_in_app_bar.dart';
import '../../../../widgets/buttons/rounded_button.dart';
import '../../../../widgets/cards/room_card/room_card.dart';
import '../../../models/entities/room.dart';
import '../../../models/types/action_type.dart';

class WebRoomsLayout extends StatefulWidget {
  final List<String> roles;
  final List<Room> rooms;

  WebRoomsLayout({
    required this.roles,
    required this.rooms,
  });

  @override
  State<WebRoomsLayout> createState() => _WebRoomsLayoutState();
}

class _WebRoomsLayoutState extends State<WebRoomsLayout> {
  @override
  Widget build(BuildContext context) {
    return ContextMenuOverlay(
      cardBuilder: (_, children) => Container(
        padding: EdgeInsets.zero,
        color: Colors.transparent,
        child: Card(
          color: Theme.of(context).primaryColorLight,
          semanticContainer: true,
          clipBehavior: Clip.antiAliasWithSaveLayer,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              15.0,
            ),
          ),
          elevation: 5,
          child: Column(
            children: children,
          ),
        ),
      ),
      buttonStyle: ContextMenuButtonStyle(
        fgColor: Theme.of(context).primaryColor,
        bgColor: Colors.transparent,
        hoverBgColor: Theme.of(context).colorScheme.secondary.withOpacity(
              0.3,
            ),
      ),
      child: Scaffold(
        appBar: HerbIOLoggedInAppBar(),
        body: widget.rooms.isEmpty
            ? widget.roles.contains('ROLE_ADMIN')
                ? Center(
                    child: RoundedButton(
                      Text('Add new Room'),
                      () {
                        executeRoomAction(
                          context: context,
                          title: 'Add new Room',
                          room: newRoom,
                          mode: ActionType.ADD,
                        );
                      },
                    ),
                  )
                : Center(
                    child: Text(
                      'No Rooms found.',
                      style: TextStyle(
                        color: Theme.of(context).primaryColorLight,
                      ),
                    ),
                  )
            : ContextMenuRegion(
                contextMenu: widget.roles.contains('ROLE_ADMIN')
                    ? GenericContextMenu(
                        buttonConfigs: [
                          ContextMenuButtonConfig(
                            'Add new Room',
                            onPressed: () => executeRoomAction(
                              context: context,
                              title: 'Add new Room',
                              room: newRoom,
                              mode: ActionType.ADD,
                            ),
                          ),
                        ],
                      )
                    : Container(),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 20,
                    vertical: 10,
                  ).r,
                  child: Center(
                    child: GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 4,
                        childAspectRatio: (1 / .75).h,
                        mainAxisSpacing: 15.0,
                      ),
                      itemCount: widget.rooms.length,
                      itemBuilder: (
                        BuildContext context,
                        int _index,
                      ) {
                        return ContextMenuRegion(
                          contextMenu: widget.roles.contains('ROLE_ADMIN')
                              ? GenericContextMenu(
                                  buttonConfigs: [
                                    ContextMenuButtonConfig(
                                      'Open',
                                      onPressed: () =>
                                          Routemaster.of(context).push(
                                        '/plants/id=${widget.rooms[_index].id}',
                                      ),
                                    ),
                                    ContextMenuButtonConfig(
                                      'Edit',
                                      onPressed: () => executeRoomAction(
                                        context: context,
                                        title: 'Edit Room',
                                        room: widget.rooms[_index],
                                        mode: ActionType.EDIT,
                                      ),
                                    ),
                                    ContextMenuButtonConfig(
                                      'Remove',
                                      onPressed: () {
                                        removeDialog(
                                          context: context,
                                          title: 'Remove Room',
                                          target: widget.rooms[_index].roomName,
                                          onPressedFunction: () {
                                            setState(
                                              () {
                                                Navigator.of(context).pop();

                                                removeRoom(
                                                  roomId:
                                                      widget.rooms[_index].id,
                                                );

                                                Routemaster.of(context).replace(
                                                  '/rooms',
                                                );
                                              },
                                            );
                                          },
                                        );
                                      },
                                    ),
                                  ],
                                )
                              : GenericContextMenu(
                                  buttonConfigs: [
                                    ContextMenuButtonConfig(
                                      'Open',
                                      onPressed: () =>
                                          Routemaster.of(context).push(
                                        '/plants/id=${widget.rooms[_index].id}',
                                      ),
                                    ),
                                  ],
                                ),
                          child: GestureDetector(
                            child: RoomCard(
                              index: _index,
                              rooms: widget.rooms,
                            ),
                            onTap: () {
                              Routemaster.of(context).push(
                                '/plants/id=${widget.rooms[_index].id}',
                              );
                            },
                          ),
                        );
                      },
                    ),
                  ),
                ),
              ),
      ),
    );
  }
}

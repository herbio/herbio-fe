import 'package:flutter/material.dart';

import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import '../../../widgets/appbar/herbio_mobile_app_bar.dart';
import '../../../utils/placeholder_data.dart';
import '../../../utils/providers/date_provider.dart';
import '../../../utils/services/action_service.dart';
import '../../models/types/action_type.dart';
import '../../screens/logged_in_screens/rooms_screen.dart';
import '../../screens/logged_in_screens/statistics_screen.dart';
import '../../screens/logged_in_screens/user_screen.dart';

class AndroidLayout extends StatefulWidget {
  final int index;
  final List<String> roles;

  AndroidLayout({
    required this.index,
    required this.roles,
  });

  @override
  State<AndroidLayout> createState() => _AndroidLayoutState(this.index);
}

class _AndroidLayoutState extends State<AndroidLayout> {
  int _currentIndex;

  _AndroidLayoutState(this._currentIndex);

  GlobalKey<CurvedNavigationBarState> _bottomNavigationKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    const Color secondaryColor = const Color(0xFF16A085);

    DateProvider _dateProvider = Provider.of<DateProvider>(context);

    void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
      if (args.value is PickerDateRange) {
        _dateProvider.setStartDate(args.value.startDate);
        _dateProvider.setEndDate(args.value.endDate ?? args.value.startDate);
        _dateProvider.notifyListeners();
      }
    }

    Widget getDateRangePicker() {
      return Container(
        height: 300,
        width: 300,
        child: SfDateRangePicker(
          todayHighlightColor: secondaryColor,
          selectionColor: secondaryColor,
          startRangeSelectionColor: secondaryColor,
          rangeSelectionColor: secondaryColor.withOpacity(0.2),
          endRangeSelectionColor: secondaryColor,
          view: DateRangePickerView.month,
          selectionMode: DateRangePickerSelectionMode.range,
          onSelectionChanged: _onSelectionChanged,
          minDate: DateTime(2022, 5, 9, 0, 0, 0),
          maxDate: DateTime.now(),
        ),
      );
    }

    return Scaffold(
      extendBody: true,
      appBar: HerbIOMobileAppBar(),
      body: [RoomsScreen(), StatisticsScreen(), UserScreen()][_currentIndex],
      bottomNavigationBar: CurvedNavigationBar(
        key: _bottomNavigationKey,
        index: this.widget.index,
        color: Theme.of(context).primaryColor,
        backgroundColor: Colors.transparent,
        items: <Widget>[
          Tooltip(
            message: 'Rooms',
            child: Icon(
              Icons.yard,
              size: 30,
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
          Tooltip(
            message: 'Statistics',
            child: Icon(
              Icons.query_stats,
              size: 30,
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
          Tooltip(
            message: 'User',
            child: Icon(
              Icons.perm_identity,
              size: 30,
              color: Theme.of(context).colorScheme.secondary,
            ),
          ),
        ],
        animationCurve: Curves.easeInOut,
        animationDuration: Duration(milliseconds: 600),
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        letIndexChange: (index) => true,
      ),
      floatingActionButton: (_currentIndex == 0 &&
              widget.roles.contains('ROLE_ADMIN'))
          ? FloatingActionButton(
              backgroundColor: Theme.of(context).primaryColor,
              child: Icon(
                Icons.add,
                color: Theme.of(context).colorScheme.secondary,
              ),
              tooltip: 'Add New Room',
              onPressed: () {
                executeRoomAction(
                  context: context,
                  title: 'Add new Room',
                  room: newRoom,
                  mode: ActionType.ADD,
                );
              },
            )
          : _currentIndex == 1
              ? SpeedDial(
                  overlayColor: Colors.transparent,
                  overlayOpacity: 0.3,
                  backgroundColor: Theme.of(context).primaryColor,
                  child: Icon(
                    Icons.filter_alt,
                    color: Theme.of(context).colorScheme.secondary,
                  ),
                  children: [
                    SpeedDialChild(
                      child: const Icon(
                        Icons.event,
                      ),
                      backgroundColor: Theme.of(context).primaryColor,
                      foregroundColor: Theme.of(context).colorScheme.secondary,
                      label: 'Select Date',
                      labelStyle: TextStyle(
                        color: Theme.of(context).colorScheme.secondary,
                      ),
                      labelBackgroundColor: Theme.of(context).primaryColor,
                      labelShadow: [
                        BoxShadow(
                          color: Theme.of(context).colorScheme.secondary,
                        ),
                      ],
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(
                                    25.0,
                                  ),
                                ),
                              ),
                              title: Center(child: const Text('Select Date')),
                              content: Container(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    getDateRangePicker(),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Text(
                                        'Confirm',
                                        style: TextStyle(
                                          color: Theme.of(context)
                                              .colorScheme
                                              .secondary,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      },
                    ),
                  ],
                )
              : null,
    );
  }
}

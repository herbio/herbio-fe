import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../../utils/services/shared_preferences_service.dart';
import '../../layouts/logged_in_layouts/android_layout.dart';
import '../../layouts/logged_out_layouts/landing/android_landing_layout.dart';
import '../../layouts/logged_out_layouts/landing/web_landing_layout.dart';
import '../loading_screen.dart';
import '../logged_in_screens/rooms_screen.dart';
import '../screen.dart';

class HomeScreen extends Screen {
  final int index;

  HomeScreen(this.index) {
    routeName = '/';
  }

  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<String>?>(
      future: getRoles(),
      builder: (BuildContext context, AsyncSnapshot<List<String>?> snapshot) {
        while (snapshot.connectionState == ConnectionState.waiting) {
          return LoadingScreen();
        }

        if (snapshot.hasData) {
          return kIsWeb
              ? RoomsScreen()
              : AndroidLayout(
                  index: widget.index,
                  roles: snapshot.data!,
                );
        } else {
          return kIsWeb ? WebLandingLayout() : AndroidLandingLayout();
        }
      },
    );
  }
}

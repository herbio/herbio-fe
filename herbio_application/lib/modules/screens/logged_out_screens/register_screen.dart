import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../../utils/services/shared_preferences_service.dart';
import '../../layouts/logged_out_layouts/register/android_register_layout.dart';
import '../../layouts/logged_out_layouts/register/web_register_layout.dart';

import '../screen.dart';
import 'home_screen.dart';

class RegisterScreen extends Screen {
  RegisterScreen() {
    routeName = '/register';
  }

  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String?>(
      future: getToken(),
      builder: (BuildContext context, AsyncSnapshot<String?> snapshot) {
        // if (snapshot.connectionState == ConnectionState.waiting) {
        //   return LoadingScreen();
        // }

        if (snapshot.hasData) {
          return HomeScreen(0);
        } else {
          return kIsWeb ? WebRegisterLayout() : AndroidRegisterLayout();
        }
      },
    );
  }
}

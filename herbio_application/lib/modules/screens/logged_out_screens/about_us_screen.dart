import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../../utils/services/shared_preferences_service.dart';
import '../../layouts/logged_out_layouts/about_us/android_about_us_layout.dart';
import '../../layouts/logged_out_layouts/about_us/web_about_us_layout.dart';
import '../screen.dart';
import 'home_screen.dart';

class AboutUsScreen extends Screen {
  AboutUsScreen() {
    routeName = '/about-us';
  }

  _AboutUsScreenState createState() => _AboutUsScreenState();
}

class _AboutUsScreenState extends State<AboutUsScreen> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<String>?>(
      future: getRoles(),
      builder: (BuildContext context, AsyncSnapshot<List<String>?> snapshot) {
        // if (snapshot.connectionState == ConnectionState.waiting) {
        //   return LoadingScreen();
        // }

        if (snapshot.hasData) {
          return HomeScreen(0);
        } else {
          return kIsWeb ? WebAboutUsLayout() : AndroidAboutUsLayout();
        }
      },
    );
  }
}

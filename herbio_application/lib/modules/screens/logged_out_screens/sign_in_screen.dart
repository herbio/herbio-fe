import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../../utils/services/shared_preferences_service.dart';
import '../../layouts/logged_out_layouts/sign_in/android_sign_in_layout.dart';
import '../../layouts/logged_out_layouts/sign_in/web_sign_in_layout.dart';
import '../loading_screen.dart';
import '../screen.dart';
import 'home_screen.dart';

class SignInScreen extends Screen {
  SignInScreen() {
    routeName = '/sign-in';
  }

  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String?>(
      future: getToken(),
      builder: (BuildContext context, AsyncSnapshot<String?> snapshot) {
        while (snapshot.connectionState == ConnectionState.waiting) {
          return LoadingScreen();
        }

        if (snapshot.hasData) {
          return HomeScreen(0);
        } else {
          return kIsWeb ? WebSignInLayout() : AndroidSignInLayout();
        }
      },
    );
  }
}

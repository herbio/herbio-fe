import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../../utils/services/http_request_services/combined_request_service.dart';
import '../../layouts/logged_in_layouts/rooms/android_rooms_layout.dart';
import '../../layouts/logged_in_layouts/rooms/web_rooms_layout.dart';
import '../loading_screen.dart';
import '../logged_out_screens/sign_in_screen.dart';
import '../screen.dart';

class RoomsScreen extends Screen {
  RoomsScreen() {
    routeName = '/rooms';
  }

  _RoomsScreenState createState() => _RoomsScreenState();
}

class _RoomsScreenState extends State<RoomsScreen> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Map>(
      future: getAllRoomsAndRoles(),
      builder: (
        BuildContext context,
        AsyncSnapshot<Map> snapshot,
      ) {
        while (snapshot.connectionState == ConnectionState.waiting) {
          return LoadingScreen();
        }

        if (snapshot.hasData) {
          return kIsWeb
              ? WebRoomsLayout(
                  roles: snapshot.data!['roles'],
                  rooms: snapshot.data!['rooms'],
                )
              : AndroidRoomsLayout(
                  roles: snapshot.data!['roles'],
                  rooms: snapshot.data!['rooms'],
                );
        } else {
          return SignInScreen();
        }
      },
    );
  }
}

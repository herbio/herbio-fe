import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../layouts/logged_in_layouts/statistics/android_statistics_layout.dart';
import '../../../utils/services/http_request_services/combined_request_service.dart';
import '../../layouts/logged_in_layouts/statistics/web_statistics_layout.dart';
import '../loading_screen.dart';
import '../screen.dart';

class StatisticsScreen extends Screen {
  StatisticsScreen() {
    routeName = '/statistics';
  }

  _StatisticsScreenState createState() => _StatisticsScreenState();
}

class _StatisticsScreenState extends State<StatisticsScreen> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Map<dynamic, dynamic>>(
      future: getAllRoomsAndRoles(),
      builder: (
        BuildContext context,
        AsyncSnapshot<Map<dynamic, dynamic>> snapshot,
      ) {
        while (snapshot.connectionState == ConnectionState.waiting) {
          return LoadingScreen();
        }
        return kIsWeb
            ? WebStatisticsLayout(
                rooms: snapshot.data!['rooms'],
              )
            : AndroidStatisticsLayout(
                rooms: snapshot.data!['rooms'],
              );
      },
    );
  }
}

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../../utils/services/shared_preferences_service.dart';
import '../../layouts/logged_in_layouts/user/android_user_layout.dart';
import '../../layouts/logged_in_layouts/user/web_user_layout.dart';
import '../screen.dart';

class UserScreen extends Screen {
  UserScreen() {
    routeName = '/user';
  }

  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<String>?>(
      future: getRoles(),
      builder: (
        BuildContext context,
        AsyncSnapshot<List<String>?> snapshot,
      ) {
        return kIsWeb ? WebUserLayout() : AndroidUserLayout();
      },
    );
  }
}

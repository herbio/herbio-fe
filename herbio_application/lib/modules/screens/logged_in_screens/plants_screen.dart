import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../../../utils/services/http_request_services/combined_request_service.dart';
import '../../layouts/logged_in_layouts/plants/android_plants_layout.dart';
import '../../layouts/logged_in_layouts/plants/web_plants_layout.dart';
import '../loading_screen.dart';
import '../logged_out_screens/sign_in_screen.dart';
import '../screen.dart';

class PlantsScreen extends Screen {
  final String id;

  PlantsScreen({required this.id}) {
    routeName = '/plants';
  }

  _PlantsScreenState createState() => _PlantsScreenState();
}

class _PlantsScreenState extends State<PlantsScreen> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Map>(
      future: getRoomContent(
        roomId: int.parse(
          widget.id.split('=')[1],
        ),
      ),
      builder: (
        BuildContext context,
        AsyncSnapshot<Map> snapshot,
      ) {
        while (snapshot.connectionState == ConnectionState.waiting) {
          return LoadingScreen();
        }

        if (snapshot.hasData) {
          return kIsWeb
              ? WebPlantsLayout(
                  roles: snapshot.data!['roles'],
                  plants: snapshot.data!['plants'],
                  waterTank: snapshot.data!['waterTank'],
                  roomId: int.parse(
                    widget.id.split('=')[1],
                  ),
                )
              : AndroidPlantsLayout(
                  roles: snapshot.data!['roles'],
                  plants: snapshot.data!['plants'],
                  waterTank: snapshot.data!['waterTank'],
                  roomId: int.parse(
                    widget.id.split('=')[1],
                  ),
                );
        } else {
          return SignInScreen();
        }
      },
    );
  }
}

import 'package:flutter/material.dart';

abstract class Screen extends StatefulWidget {
  late final String routeName;

  String getRouteName() => this.routeName;
}

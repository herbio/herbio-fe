import 'package:flutter/material.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Center(
        child: Container(
          child: SpinKitDoubleBounce(
            color: Theme.of(context).colorScheme.secondary,
            size: 50.0,
          ),
        ),
      ),
    );
  }
}

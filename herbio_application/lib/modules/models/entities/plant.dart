import '../types/watering_mode.dart';

class Plant {
  final int id;
  final String plantName;
  final int wateringRequirement;
  final int lightingRequirement;
  final double humidity;
  final double soilTemperature;
  final WateringMode wateringMode;
  final int wateringAmount;
  final double lowerWaterThreshold;
  final double upperWaterThreshold;

  const Plant({
    required this.id,
    required this.plantName,
    required this.wateringRequirement,
    required this.lightingRequirement,
    required this.humidity,
    required this.soilTemperature,
    required this.wateringMode,
    required this.wateringAmount,
    required this.lowerWaterThreshold,
    required this.upperWaterThreshold,
  });
}

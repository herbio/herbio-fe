class WaterTank {
  final int id;
  final double capacity;
  final double waterLevel;
  final DateTime lastTimeFilled;
  final double shortestCycle;
  final double longestCycle;
  final double lastCycle;

  const WaterTank({
    required this.id,
    required this.capacity,
    required this.waterLevel,
    required this.lastTimeFilled,
    required this.shortestCycle,
    required this.longestCycle,
    required this.lastCycle,
  });
}

import '../notifications/herbio_notification.dart';
import 'plant.dart';
import 'water_tank.dart';

class Room {
  final int id;
  late String roomName;
  final int lightingLevel;
  final WaterTank waterTank;
  final double temperature;
  final List<Plant> plants;
  List<HerbioNotification> notifications;

  Room({
    required this.id,
    required this.roomName,
    required this.lightingLevel,
    required this.waterTank,
    required this.temperature,
    required this.plants,
    required this.notifications,
  });
}

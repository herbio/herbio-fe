class LineChartData {
  final String timeIndicator;
  final Map<String, double> values;

  LineChartData(this.timeIndicator, this.values);
}

import 'package:flutter/widgets.dart';

import '../types/notification_type.dart';

class HerbioNotification {
  final Icon icon;
  final NotificationType type;
  final String notificationText;

  const HerbioNotification({
    required this.icon,
    required this.type,
    required this.notificationText,
  });
}

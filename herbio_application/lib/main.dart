import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:routemaster/routemaster.dart';
import 'package:universal_html/html.dart';

import 'config/url_strategies/url_strategy_noop.dart'
    if (dart.library.html) 'config/url_strategies/url_strategy_web.dart';
import 'modules/screens/logged_in_screens/plants_screen.dart';
import 'modules/screens/logged_in_screens/rooms_screen.dart';
import 'modules/screens/logged_in_screens/statistics_screen.dart';
import 'modules/screens/logged_out_screens/about_us_screen.dart';
import 'modules/screens/logged_out_screens/home_screen.dart';
import 'modules/screens/logged_out_screens/register_screen.dart';
import 'modules/screens/logged_out_screens/sign_in_screen.dart';
import 'utils/providers/date_provider.dart';
import 'utils/providers/image_path_provider.dart';
import 'utils/providers/lighting_requirements_provider.dart';
import 'utils/providers/password_visibility_provider.dart';
import 'utils/providers/watering_mode_provider.dart';

void main() {
  usePathUrlStrategy();

  if (kIsWeb) {
    document.onContextMenu.listen((evt) => evt.preventDefault());
  }

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => DateProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => PasswordVisibilityProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => LightingRequirementsProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => WateringModeProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => ImagePathProvider(),
        ),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  final ThemeData theme = ThemeData();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);

    final routes = RouteMap(
      routes: {
        '/': (_) => MaterialPage(child: HomeScreen(0)),
        '/about-us': (_) => MaterialPage(child: AboutUsScreen()),
        '/home/:id': (info) => MaterialPage(
              child: HomeScreen(
                int.parse(info.pathParameters['id']!),
              ),
            ),
        '/register': (_) => MaterialPage(child: RegisterScreen()),
        '/sign-in': (_) => MaterialPage(child: SignInScreen()),
        '/rooms': (_) => MaterialPage(child: RoomsScreen()),
        '/plants/:id': (info) =>
            MaterialPage(child: PlantsScreen(id: info.pathParameters['id']!)),
        '/statistics': (_) => MaterialPage(child: StatisticsScreen()),
      },
    );

    return ScreenUtilInit(
      designSize: kIsWeb ? Size(1920, 974) : Size(393, 836),
      builder: (child) {
        return MaterialApp.router(
          debugShowCheckedModeBanner: false,
          title: 'HerbIO',
          theme: theme.copyWith(
            primaryColor: const Color(0xFF0F0F0F),
            colorScheme: theme.colorScheme.copyWith(
              secondary: const Color(0xFF16A085),
            ),
            primaryColorLight: Colors.white,
            scaffoldBackgroundColor: const Color(0xFF212B2E),
            popupMenuTheme: PopupMenuThemeData(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                  15,
                ),
              ),
            ),
          ),
          routerDelegate: RoutemasterDelegate(
            routesBuilder: (context) => routes,
          ),
          routeInformationParser: RoutemasterParser(),
        );
      },
    );
  }
}

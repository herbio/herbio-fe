import 'package:flutter/material.dart';

class LayoutProvider extends ChangeNotifier {
  String _currentLayout = "home";

  String get currentLayout => _currentLayout;

  void changeCurrentLayout(String newLayout) {
    this._currentLayout = newLayout;
    notifyListeners();
  }
}
